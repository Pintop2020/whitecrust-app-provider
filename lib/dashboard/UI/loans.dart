import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:white_crust/data/login_session.dart';
import '../../data/custom_color.dart';
import '../../data/custom_sizes.dart';
import 'package:intl/intl.dart';
import '../../data/custom_strings.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/gestures.dart';
import "dart:math";
import 'package:white_crust/data/utils.dart';
import 'sub/single_loan.dart';

class LoanHistory extends StatefulWidget {
  @override
  _LoanHistory createState() => _LoanHistory();
}

class _LoanHistory extends State<LoanHistory> {

  final oCcy = new NumberFormat("#,##0.00", "en_US");
  final LoginSessionManager loginSessionManager = new LoginSessionManager();
  var _balance = 0.0;
  List<String> _frequency = ['Last 7 days', 'Last 30 days', 'Yesterday', 'Today', 'Last 14 days', 'All time'];
  var _sFrequency = 'Last 7 days';
  int touchedIndex;
  ApiBaseHelper apiBaseHelper = new ApiBaseHelper();
  List _transactions = [];
  List _recurring = [];
  var _applied = 0.0, _approved = 0.0;
  List<PieChartSectionData> _dadata = [];
  var _userId;

  void _getData() async {
    var data = jsonDecode(await loginSessionManager.getOtherData());
    if(mounted)setState(() {
      _userId = data['user']['id'];
      _balance = double.parse("${data['user']['loan_balance']}");
      _applied = double.parse("${data['user']['total_applied']}");
      _approved = double.parse("${data['user']['total_approved']}");
    });
    _loadData();
  }

  void _addPie(var received, var spent){
    _dadata.clear();
    var sum = received + spent;
    double rec = received > 0 ? (received/sum)*100 : 0;
    double spen = spent > 0 ? (spent/sum)*100 : 0;
    if(rec > 0)_dadata.add(PieChartSectionData(
      color: const Color(CustomColor.seaTurtle),
      value: rec,
      title: '',
      radius: 25,
      titleStyle: TextStyle(
          fontSize: 18, fontWeight: FontWeight.bold, color: const Color(0xff044d7c)),
      titlePositionPercentageOffset: 0.55,
    ));
    if(spen > 0)_dadata.add(PieChartSectionData(
      color: const Color(CustomColor.chestnutRed),
      value: spen,
      title: '',
      radius: 25,
      titleStyle: TextStyle(
          fontSize: 18, fontWeight: FontWeight.bold, color: const Color(0xff044d7c)),
      titlePositionPercentageOffset: 0.55,
    ));
    if(mounted)setState(() {
    });
  }

  void changeValue(String value){
    _sFrequency = value;
    if(mounted)setState(() {});
    _loadData();
  }

  _loadData({bool load: false}) async{
    var range = convertToLast(_sFrequency);
    _transactions.clear();
    _recurring.clear();
    var response = await apiBaseHelper.get(context, UrlLinks.loanHistory+"?range=$range", await getHeader(), isLoader: load);
    if(response['status']){
      _transactions = response['data'].toList();
      _addPie(_approved, _applied);
    }else showInSnackBar(context, response['message'], Color(CustomColor.white), Color(CustomColor.danger));
    if(mounted)setState(() {
    });
  }

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () {
      _getData();
    });
  }

  Widget _ddata(var data){
    var decoded = jsonDecode(data['details']);
    return GestureDetector(
      onTap: (){
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (BuildContext context) =>
                  SingleLoan(loan: data['id'], user_id: _userId)
          ),
        ).whenComplete((){
          _loadData(load: false);
        });
      },
      child: Container(
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
            border: Border(
                bottom: BorderSide(color: Color(CustomColor.black2).withOpacity(.3))
            )
        ),
        padding: EdgeInsets.symmetric(vertical: 15),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              padding: EdgeInsets.symmetric(vertical: 8, horizontal: 10),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(50),
                  color: Color(randomColor[Random().nextInt(randomColor.length)])
              ),
              child: Text(getTransName(decoded), maxLines: 1, textAlign: TextAlign.center,style: GoogleFonts.quicksand(
                textStyle: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: CustomSize.normalText,
                  color: Color(CustomColor.white),
                )
              )),
            ),
            SizedBox(width: 20),
            Expanded(
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(getTransName(decoded), style: GoogleFonts.quicksand(
                      textStyle: TextStyle(
                        fontWeight: FontWeight.w300,
                        fontSize: CustomSize.buttonText,
                        color: Color(CustomColor.black2),
                      )
                    )),
                    SizedBox(height: 3),
                    Text(getTransDesc(decoded), style: GoogleFonts.quicksand(
                      textStyle: TextStyle(
                        fontWeight: FontWeight.w300,
                        fontSize: CustomSize.smallText,
                        color: Color(CustomColor.black2),
                      )
                    )),
                  ],
                ),
              ),
            ),
            SizedBox(width: 12),
            Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("${CustomStrings.currency+oCcy.format(double.parse("${decoded['loan_info']['amount']}"))}", textAlign: TextAlign.end, style: GoogleFonts.quicksand(
                    textStyle: TextStyle(
                      fontWeight: FontWeight.w300,
                      fontSize: CustomSize.buttonText,
                      color: Color(getColorForLoan(data['status']['status'])),
                    )
                  )),
                  SizedBox(height: 3),
                  Text("${decoded['loan_info']['tenor']} months",textAlign: TextAlign.end, style: GoogleFonts.quicksand(
                    textStyle: TextStyle(
                      fontWeight: FontWeight.w300,
                      fontSize: CustomSize.smallText,
                      color: Color(getColorForLoan(data['status']['status'])),
                    )
                  )),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;
    return Scaffold(
        appBar: AppBar(
          title: Text("History", textAlign: TextAlign.center,style: GoogleFonts.quicksand(
            textStyle: TextStyle(
              fontWeight: FontWeight.w600,
              fontSize: CustomSize.buttonText,
              color: Color(CustomColor.black2),
            )
          )),
          backgroundColor: Color(CustomColor.white),
          centerTitle: true,
          iconTheme: IconThemeData(
            color: Color(CustomColor.offBlack2), //change your color here
          ),
        ),
        body: RefreshIndicator(
            onRefresh: (()=>_loadData(load: true)),
            child: Container(
                color: Color(CustomColor.white),
                height: height,
                width: width,
                child: SingleChildScrollView(
                    physics: AlwaysScrollableScrollPhysics(),
                    child: Container(
                      width: width,
                      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            decoration: BoxDecoration(
                                color: Color(CustomColor.primaryDark),
                                borderRadius: BorderRadius.circular(12)
                            ),
                            padding: EdgeInsets.only(left: 20, right: 20),
                            height: 130,
                            width: width,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text("${CustomStrings.currency}${oCcy.format(_balance)}", textAlign: TextAlign.center, style: GoogleFonts.quicksand(
                                  textStyle: TextStyle(
                                    fontWeight: FontWeight.w600,
                                    fontSize: _balance.toString().length > 4 ? CustomSize.mediumText : CustomSize.bigTitle,
                                    color: Color(CustomColor.white),
                                  )
                                )),
                                SizedBox(height: 10),
                                Text("Balance", textAlign: TextAlign.center, style: GoogleFonts.quicksand(
                                  textStyle: TextStyle(
                                    fontWeight: FontWeight.w600,
                                    fontSize: CustomSize.smallText,
                                    color: Color(CustomColor.offWhite),
                                  )
                                )),
                              ],
                            ),
                          ),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              DropdownButton<String>(
                                  isExpanded: false,
                                  value: _sFrequency,
                                  icon: Icon(Icons.arrow_drop_down, color: Color(CustomColor.black2)),
                                  style: GoogleFonts.quicksand(
                                    textStyle: TextStyle(
                                      fontWeight: FontWeight.w500,
                                      fontSize: CustomSize.buttonText,
                                      color: Color(CustomColor.black2),
                                    )
                                  ),
                                  iconSize: 20,
                                  underline: SizedBox(),
                                  onChanged: changeValue,
                                  items: _frequency.map((String value) {
                                    return new DropdownMenuItem<String>(
                                      value: value,
                                      child: new Text(value, style: GoogleFonts.quicksand(
                                        textStyle: TextStyle(
                                          fontSize: CustomSize.buttonText,
                                          color: Color(CustomColor.black2),
                                        )
                                      )),
                                    );
                                  }).toList()
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              if(_applied > 0 || _approved > 0)Expanded(
                                child: AspectRatio(
                                    aspectRatio: 1,
                                    child: PieChart(
                                      PieChartData(
                                          pieTouchData: PieTouchData(touchCallback: (pieTouchResponse) {
                                            if(mounted)setState(() {
                                              final desiredTouch = pieTouchResponse.touchInput is! PointerExitEvent &&
                                                  pieTouchResponse.touchInput is! PointerUpEvent;
                                              if (desiredTouch && pieTouchResponse.touchedSection != null) {
                                                touchedIndex = pieTouchResponse.touchedSection.touchedSectionIndex;
                                              } else {
                                                touchedIndex = -1;
                                              }
                                            });
                                          }),
                                          borderData: FlBorderData(
                                            show: false,
                                          ),
                                          sectionsSpace: 0,
                                          centerSpaceRadius: 20,
                                          sections: _dadata
                                      ),
                                    )
                                ),
                              ),
                              SizedBox(width: 15),
                              Expanded(
                                child: Container(
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text("Total Application Sent", style: GoogleFonts.quicksand(
                                        textStyle: TextStyle(
                                          fontWeight: FontWeight.w500,
                                          fontSize: CustomSize.smallText,
                                          color: Color(CustomColor.black2).withOpacity(.8),
                                        )
                                      )),
                                      Text("${CustomStrings.currency+oCcy.format(_applied)}", style: GoogleFonts.quicksand(
                                        textStyle: TextStyle(
                                          fontWeight: FontWeight.w600,
                                          fontSize: CustomSize.mediumTextMin,
                                          color: Color(CustomColor.seaTurtle),
                                        )
                                      )),
                                      SizedBox(height: 10),
                                      Text("Total Approved", style: GoogleFonts.quicksand(
                                        textStyle: TextStyle(
                                          fontWeight: FontWeight.w500,
                                          fontSize: CustomSize.smallText,
                                          color: Color(CustomColor.black2).withOpacity(.8),
                                        )
                                      )),
                                      Text("${CustomStrings.currency+oCcy.format(_approved)}", style: GoogleFonts.quicksand(
                                        textStyle: TextStyle(
                                          fontWeight: FontWeight.w600,
                                          fontSize: CustomSize.mediumTextMin,
                                          color: Color(CustomColor.chestnutRed),
                                        )
                                      ))
                                    ],
                                  ),
                                ),
                              )
                            ],
                          ),
                          if(_transactions != null && _transactions.length > 0) for(int i=0;i<_transactions.length;i++) _ddata(_transactions[i])
                        ],
                      ),
                    )
                )
            )
        )
    );
  }
}