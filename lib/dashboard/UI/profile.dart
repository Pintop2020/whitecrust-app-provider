import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:white_crust/dashboard/UI/loans.dart';
import 'package:white_crust/data/login_session.dart';
import 'package:white_crust/data/utils.dart';
import 'dart:convert';
import '../../data/custom_sizes.dart';
import '../../data/custom_color.dart';
import 'package:intl/intl.dart';
import '../../home.dart';
import 'package:cached_network_image/cached_network_image.dart';


class ProfilePage extends StatefulWidget {
  ProfilePage({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _ProfilePage createState() => _ProfilePage();
}

class _ProfilePage extends State<ProfilePage> {

  final oCcy = new NumberFormat("#,##0.00", "en_US");
  LoginSessionManager loginSessionManager = new LoginSessionManager();
  ApiBaseHelper apiBaseHelper = new ApiBaseHelper();
  var _fullData;

  _getCaches() async {
    var data = await loginSessionManager.getOtherData();
    setState(() {
      _fullData = jsonDecode(data);
    });
  }

  void _signOut() async {
    var decoded = await apiBaseHelper.get(context, UrlLinks.logout, await getHeader(), isLoader: true);
    if(decoded['status']) {
      loginSessionManager.logout();
      Navigator.pushAndRemoveUntil(context, MaterialPageRoute(
        builder: (BuildContext context) => Home(),
      ), (r) => false);
    }
  }

  Widget _actions({String desc, bool showOther: false, String other, GestureTapCallback onTap}){
    return GestureDetector(
      onTap: onTap,
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
        decoration: BoxDecoration(
          border: Border(
              bottom: BorderSide(color: Color(CustomColor.offBlack).withOpacity(.1))
          ),
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(desc, textAlign: TextAlign.center,style: GoogleFonts.quicksand(
              textStyle: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: CustomSize.buttonText,
                color: Color(CustomColor.black),
              )
            )),
            Spacer(),
            if(showOther) Text(other, textAlign: TextAlign.center,style: GoogleFonts.quicksand(
              textStyle: TextStyle(
                fontWeight: FontWeight.w500,
                fontStyle: FontStyle.italic,
                fontSize: CustomSize.buttonText,
                color: Color(CustomColor.black2).withOpacity(.5),
              )
            )),
            if(showOther) SizedBox(width: 5),
            Icon(Icons.arrow_forward_ios, size: 12, color: Color(CustomColor.black))
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;
    return Scaffold(
        appBar: AppBar(
          title: Text("My Profile", textAlign: TextAlign.center,style: GoogleFonts.quicksand(
            textStyle: TextStyle(
              fontWeight: FontWeight.w700,
              fontSize: CustomSize.normalText,
              color: Color(CustomColor.white),
            )
          )),
          backgroundColor: Color(CustomColor.primaryDark),
          elevation: 0.0,
          iconTheme: IconThemeData(
            color: Color(CustomColor.white), //change your color here
          ),
        ),
        body: Container(
          width: width,
          height: height,
          color: Color(CustomColor.offWhite),
          child: SingleChildScrollView(
              child: Container(
                  width: width,
                  height: height,
                  color: Color(CustomColor.offWhite),
                  child: _fullData != null ? Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        width: width,
                        child: Column(
                          children: [
                            Container(
                              color: Color(CustomColor.primaryDark),
                              width: width,
                              padding: EdgeInsets.only(top: 40, bottom: 20),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Container(
                                    width: 90,
                                    height: 90,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(90),
                                      border: Border.all(width: 5, color: Color(CustomColor.offWhite)),
                                      image: DecorationImage(
                                          image: CachedNetworkImageProvider(_fullData['user']['profile_photo_url']),
                                          fit: BoxFit.cover
                                      ),
                                      boxShadow: [
                                        BoxShadow(
                                          color: Colors.grey.withOpacity(0.5),
                                          spreadRadius: 5,
                                          blurRadius: 7,
                                          offset: Offset(0, 3), // changes position of shadow
                                        ),
                                      ],
                                    ),
                                  ),
                                  SizedBox(height: 20),
                                  Text("${_fullData['user']['first_name']} ${_fullData['user']['last_name']} ${_fullData['user']['other_name']}", textAlign: TextAlign.center,
                                      style: GoogleFonts.quicksand(
                                        textStyle: TextStyle(
                                          fontWeight: FontWeight.w500,
                                          fontSize: CustomSize.bigText,
                                          color: Color(CustomColor.white),
                                        )
                                      )),
                                  SizedBox(height: 12),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      _actions(desc: "Mobile", other: "+234${_fullData['user']['mobile']}", showOther: true, onTap: null),
                      _actions(desc: "Email", other: "${_fullData['user']['email']}", showOther: true, onTap: null),
                      _actions(desc: "History", onTap: (){
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (BuildContext context) => LoanHistory(),
                          ),
                        );
                      }),
                      _actions(desc: "Sign Out", onTap: _signOut),
                    ],
                  ) : null,
              )
          )
        )
    );
  }

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () {
      _getCaches();
    });
  }
}