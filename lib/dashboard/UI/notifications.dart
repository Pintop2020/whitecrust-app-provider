import 'package:flutter/services.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter/rendering.dart';
import 'package:white_crust/data/custom_color.dart';
import 'package:white_crust/data/custom_sizes.dart';
import 'package:white_crust/data/utils.dart';

class NotificationsPage extends StatefulWidget {
  @override
  _NotificationsPage createState() => _NotificationsPage();
}

class _NotificationsPage extends State<NotificationsPage> {

  List<Notifications> _notifications = [];
  List<Notifications> items = [];
  int perPage  = 0;
  int present = 0;
  ApiBaseHelper apiBaseHelper = new ApiBaseHelper();

  _validateData() async {
    var response = await apiBaseHelper.get(context, UrlLinks.readNotif, await getHeader(), isLoader: true);
    if(response['status']){
      _notifications.clear();
      items.clear();
      perPage = 0;
      present = 0;
      for(Map i in response['data']){
        _notifications.add(Notifications.fromJson(i));
      }
      perPage = _notifications.length >= 10 ? 10 : _notifications.length;
      items.addAll(_notifications.getRange(present, present + perPage));
      present = present + perPage;
      if(mounted)setState(() { });
    }
  }

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () {
      _validateData();
    });
  }

  Widget _empty(){
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;
    return Container(
        width: width,
        height: height,
        margin: EdgeInsets.only(top: 200),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Image.asset("assets/images/empty_notification.png", width: 200),
            SizedBox(height: 20),
            Text("No notification found!", textAlign: TextAlign.center,style: GoogleFonts.quicksand(
                textStyle: TextStyle(
                  fontWeight: FontWeight.w700,
                  fontSize: CustomSize.normalText,
                  color: Color(CustomColor.black2),
                )
            )),
          ],
        )
    );
  }

  Widget _singleItem(Notifications data){
    final width = MediaQuery.of(context).size.width-110;
    return Container(
      width: width,
      padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
      child: Container(
          padding: EdgeInsets.only(bottom: 20),
          decoration: BoxDecoration(
              border: Border(
                  bottom: BorderSide(color: Color(CustomColor.offBlack).withOpacity(.2))
              )
          ),
          width: width,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(data.data.title, style: GoogleFonts.quicksand(
                      textStyle: TextStyle(
                        fontWeight: FontWeight.w500,
                        fontSize: CustomSize.buttonText,
                        color: Color(CustomColor.black2),
                      )
                  )),
                  Spacer(),
                  Text(getTimeConverted(data.created_at), style: GoogleFonts.ubuntuMono(
                      textStyle: TextStyle(
                        fontWeight: FontWeight.w500,
                        fontSize: CustomSize.smallText,
                        color: Color(CustomColor.black2),
                      )
                  )),
                ],
              ),
              SizedBox(height: 10),
              Text("${data.data.body}", style: GoogleFonts.ubuntu(
                  textStyle: TextStyle(
                    fontSize: CustomSize.normalText,
                    color: Color(CustomColor.black2).withOpacity(.7),
                  )
              )),
            ],
          )
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;
    return AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.dark,
        child: Scaffold(
            appBar: AppBar(
              centerTitle: false,
              title: Text("Notifications", textAlign: TextAlign.center,style: GoogleFonts.quicksand(
                  textStyle: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: CustomSize.normalText,
                    color: Color(CustomColor.black2),
                  )
              )),
              backgroundColor: Color(CustomColor.white),
              iconTheme: IconThemeData(
                color: Color(CustomColor.primary), //change your color here
              ),
            ),
            bottomNavigationBar: present < _notifications.length && _notifications.length > 10 ? Container(
              color: Color(CustomColor.primary),
              child: TextButton(
                child: Text("Load More", style: GoogleFonts.ubuntu(
                    textStyle: TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: CustomSize.normalText,
                      color: Color(CustomColor.white),
                    )
                )),
                onPressed: () {
                  if(mounted)setState(() {
                    if((present + perPage)> _notifications.length) {
                      items.addAll(
                          _notifications.getRange(present, _notifications.length));
                    } else {
                      items.addAll(
                          _notifications.getRange(present, present + perPage));
                    }
                    present = present + perPage;
                  });
                },
              ),
            ) : null,
            body: Container(
              color: Color(CustomColor.white),
              height: height,
              width: width,
              child: _notifications != null && _notifications.length > 0 ? SizedBox(
                width: width,
                height: height,
                child: ListView.builder(
                  itemCount: (present <= _notifications.length) ? items.length + 1 : items.length,
                  itemBuilder: (context, index) {
                    return (index == items.length ) ?
                    Container()
                        :
                    _singleItem(items[index]);
                  },
                ),
              ) : _empty(),
            )
        )
    );
  }
}