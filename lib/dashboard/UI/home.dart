import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:badges/badges.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:white_crust/data/custom_icons_icons.dart';
import 'package:white_crust/data/login_session.dart';
import 'package:white_crust/data/custom_color.dart';
import 'package:white_crust/data/custom_sizes.dart';
import 'package:white_crust/data/custom_strings.dart';
import 'package:flutter/cupertino.dart';
import 'notifications.dart';
import 'take_loan.dart';
import 'package:white_crust/data/utils.dart';
import 'package:white_crust/dashboard/UI/sub/single_loan.dart';
import 'package:white_crust/dashboard/UI/sub/support.dart';
import 'package:intl/intl.dart';

class Dashboard extends StatefulWidget {
  @override
  _Dashboard createState() => _Dashboard();
}

class _Dashboard extends State<Dashboard> {

  final oCcy = new NumberFormat("#,##0.00", "en_US");
  var _data;
  List _loans = [];
  bool _active = false, _pending = false, _declined = false, _pendingSignature = false, _approved =false, _offerPending;
  var _loan;
  LoginSessionManager loginSessionManager = new LoginSessionManager();
  Timer timer;
  int _notifications = 0;

  void getData() async{
    _active = _declined = _pending = _pendingSignature = _approved = _offerPending = false;
    var data = jsonDecode(await loginSessionManager.getOtherData());
    _data = data;
    _loans = data['user']['loans'].toList();
    _notifications = data['unread_notifications'];
    if(_loans.length > 0){
      _loan = _loans[_loans.length-1];
      if(_loan['status']['status'] == 'pending') {
        _pending = true;
        if(_loan['signature_link'] != '') _pendingSignature = true;
      }else if(_loan['status']['status'] == 'active') {
        _active = true;
      }else if(_loan['status']['status'] == 'approved') {
        _approved = true;
        if(_loan['offer'] != null && _loan['offer']['signature'] == null){
          _offerPending = true;
        }
      }else if(_loan['status']['status'] == 'declined') {
        _declined = true;
      }
    }
    setState(() {
    });
  }


  @override
  void initState() {
    super.initState();
    getData();
    timer = Timer.periodic(Duration(seconds: 20), (Timer t) => getData());
  }

  @override
  void dispose() {
    timer?.cancel();
    super.dispose();
  }

  Widget _noLoan(){
    final width = MediaQuery.of(context).size.width;
    return Card(
      elevation: 10.0,
      shape: BeveledRectangleBorder(
        borderRadius: BorderRadius.circular(5.0),
      ),
      child: Container(
        width: width,
        padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Image.asset("assets/images/home_banner_1.png", width: 200),
            SizedBox(height: 20),
            Text("${Platform.isIOS ? 'Cash':'Loans'} up to ${CustomStrings.currency}5,000,000", textAlign: TextAlign.center, style: GoogleFonts.quicksand(
              textStyle: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: CustomSize.mediumText,
                color: Color(CustomColor.primary),
              )
            )),
            Text("You can request for ${Platform.isIOS ? 'an offer':'a loan'} of ${CustomStrings.currency}50,000 to ${CustomStrings.currency}5,000,000 if you work "
                "in the public sector.", textAlign: TextAlign.center, style: GoogleFonts.quicksand(
              textStyle: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: CustomSize.normalText,
                color: Color(CustomColor.black2),
              )
            )),
            SizedBox(height: 20),
            GestureDetector(
                onTap: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) =>
                            TakeLoan()
                    ),
                  );
                },
                child: Container(
                  padding: EdgeInsets.symmetric(vertical: 12, horizontal: 20),
                  decoration: BoxDecoration(
                    color: Color(CustomColor.primary),
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: Center(
                    child: Text("REQUEST ${Platform.isIOS ? 'CASH':'LOAN'}", style: GoogleFonts.quicksand(
                      textStyle: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: CustomSize.buttonText,
                        color: Color(CustomColor.white),
                      )
                    )),
                  ),
                )
            ),
          ],
        ),
      ),
    );
  }

  Widget _pendingSignatureP(){
    final width = MediaQuery.of(context).size.width;
    return Container(
      width: width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("Your ${Platform.isIOS ? 'offer':'loan'} is pending your signature.", style: GoogleFonts.quicksand(
            textStyle: TextStyle(
              fontWeight: FontWeight.w600,
              fontSize: CustomSize.buttonText,
              color: Color(CustomColor.black2).withOpacity(.5),
            )
          )),
          SizedBox(height: 20),
          Text("Our officers will start processing your ${Platform.isIOS ? 'offer':'loan'} once you append your signatures. Your signatures are used for mandate generation and attestations.", style: TextStyle(
              fontWeight: FontWeight.w600,
              fontSize: CustomSize.normalText,
              color: Color(CustomColor.black2),
              fontFamily: 'Poppins'
          )),
          SizedBox(height: 20),
          TextButton(onPressed: (){
            launchURL("${_loan['signature_link']}/${_data['user']['id']}");
          }, child: Text("SIGN ${Platform.isIOS ? 'APPLICATION':'LOAN'} FORM", style: GoogleFonts.quicksand(
            textStyle: TextStyle(
              fontWeight: FontWeight.w600,
              fontSize: CustomSize.bigText,
              color: Color(CustomColor.primary),
            )
          )),)
        ],
      )
    );
  }

  Widget _pendingSignatureO(){
    final width = MediaQuery.of(context).size.width;
    return Container(
        width: width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("Your offer letter is pending your signature.", style: GoogleFonts.quicksand(
              textStyle: TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: CustomSize.buttonText,
                color: Color(CustomColor.black2).withOpacity(.5),
              )
            )),
            SizedBox(height: 20),
            Text("An offer letter has been created for your application and is pending your signature. "
                "Our officer will proceed to disbursing your ${Platform.isIOS ? 'cash':'loan'} once your signature has been attached.", style: GoogleFonts.quicksand(
              textStyle: TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: CustomSize.normalText,
                color: Color(CustomColor.black2),
              )
            )),
            SizedBox(height: 20),
            TextButton(onPressed: (){
              launchURL("${_loan['offer']['signature_link']}/${_data['user']['id']}");
            }, child: Text("SIGN OFFER LETTER", style: GoogleFonts.quicksand(
              textStyle: TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: CustomSize.bigText,
                color: Color(CustomColor.primary),
              )
            )),)
          ],
        )
    );
  }

  Widget _pendingLoan(){
    final width = MediaQuery.of(context).size.width;
    return Container(
      width: width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("Your ${Platform.isIOS ? 'offer':'loan'} is being processed.", style: GoogleFonts.quicksand(
              textStyle: TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: CustomSize.buttonText,
                color: Color(CustomColor.black2).withOpacity(.5),
              )
            )),
            SizedBox(height: 20),
            Text("Your ${Platform.isIOS ? 'offer':'loan'} is being processed, please always check back to check the status of your request"
                ".\nYou will get your funds in your account once we approve your ${Platform.isIOS ? 'offer':'loan'}.", style:
            GoogleFonts.quicksand(
              textStyle: TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: CustomSize.normalText,
                color: Color(CustomColor.black2),
              )
            )),
            SizedBox(height: 20),
            TextButton(onPressed: (){
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) =>
                        SingleLoan(loan: _loan['id'], user_id: _data['user']['id'])
                ),
              );
            }, child: Text("VIEW ${Platform.isIOS ? 'REQUEST':'LOAN'} DETAILS", style: GoogleFonts.quicksand(
              textStyle: TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: CustomSize.bigText,
                color: Color(CustomColor.accent),
              )
            ))),
          ],
        )
    );
  }

  Widget _declinedLoan(){
    final width = MediaQuery.of(context).size.width;
    return Container(
      width: width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("Your ${Platform.isIOS ? 'offer':'loan'} has been declined.", style: GoogleFonts.quicksand(
              textStyle: TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: CustomSize.buttonText,
                color: Color(CustomColor.black2).withOpacity(.5),
              )
            )),
            SizedBox(height: 20),
            Text("You do not qualify to access ${Platform.isIOS ? 'cash':'loan'} from us. "
                "Your ${Platform.isIOS ? 'offer':'loan'} has been declined, please keep the app installed and try again in 7 days.", style: GoogleFonts.quicksand(
              textStyle: TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: CustomSize.normalText,
                color: Color(CustomColor.black2),
              )
            )),
            SizedBox(height: 20),
            TextButton(onPressed: (){
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) =>
                        SingleLoan(loan: _loan['id'], user_id: _data['user']['id'])
                ),
              );
            }, child: Text("VIEW ${Platform.isIOS ? 'OFFER':'LOAN'} DETAILS", style: GoogleFonts.quicksand(
              textStyle: TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: CustomSize.bigText,
                color: Color(CustomColor.accent),
              )
            ))),
            SizedBox(height: 20),
            GestureDetector(
                onTap: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) =>
                            TakeLoan()
                    ),
                  );
                },
                child: Container(
                  padding: EdgeInsets.symmetric(vertical: 12, horizontal: 20),
                  decoration: BoxDecoration(
                    color: Color(CustomColor.primary),
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: Center(
                    child: Text("REAPPLY NOW", style: GoogleFonts.quicksand(
                      textStyle: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: CustomSize.buttonText,
                        color: Color(CustomColor.white),
                      )
                    )),
                  ),
                )
            ),
          ],
        )
    );
  }

  Widget _activeLoan(){
    final width = MediaQuery.of(context).size.width;
    return Container(
      width: width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text("AMOUNT DUE", style: GoogleFonts.quicksand(
              textStyle: TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: CustomSize.buttonText,
                color: Color(CustomColor.black2).withOpacity(.5),
              )
            )),
            SizedBox(height: 20),
            Text("${CustomStrings.currency}${oCcy.format(_data['user']['loan_balance'])}", style: GoogleFonts.quicksand(
              textStyle: TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: CustomSize.maxMinSmall,
                color: Color(CustomColor.primary),
              )
            )),
            SizedBox(height: 20),
            Row(
              children: [
                Spacer(),
                Icon(CustomIcons.info, color: Color(CustomColor.primaryDark), size: 16),
                SizedBox(width: 10),
                // Text("Payment due in 11 days.", style: TextStyle(
                //     fontWeight: FontWeight.w600,
                //     fontSize: CustomSize.normalText,
                //     color: Color(CustomColor.black2),
                //     fontFamily: 'Poppins'
                // )),
                // Spacer(),
                TextButton(onPressed: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) =>
                            SingleLoan(loan: _loan['id'], user_id: _data['user']['id'])
                    ),
                  );
                }, child: Text("DETAILS", style: GoogleFonts.quicksand(
                  textStyle: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: CustomSize.bigText,
                    color: Color(CustomColor.primary),
                  )
                ))),
                Spacer(),
              ],
            )

          ],
        )
    );
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;
    return AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.dark,
        child: Scaffold(
            appBar: AppBar(
              backgroundColor: Color(CustomColor.white),
              // elevation: 0.0,
              title: Text("Dashboard", style: GoogleFonts.quicksand(
                textStyle: TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: CustomSize.bigText,
                  color: Color(CustomColor.black2),
                )
              )),
              centerTitle: false,
              iconTheme: IconThemeData(
                color: Color(CustomColor.primary), //change your color here
              ),
              actions: [
                _notifications != null && _notifications > 0 ? Badge(
                  position: BadgePosition.topEnd(top: 0, end: 3),
                  animationDuration: Duration(milliseconds: 300),
                  animationType: BadgeAnimationType.slide,
                  badgeContent: Text("",
                    style: TextStyle(color: Colors.white, fontSize: CustomSize.smallestTest),
                  ),
                  child: IconButton(icon: Icon(Icons.notifications_none_outlined, color: Color(CustomColor.primary).withOpacity(.6)), onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) =>
                              NotificationsPage()
                      ),
                    );
                  }),
                ) : IconButton(icon: Icon(Icons.notifications_none_outlined, color: Color(CustomColor.primary).withOpacity(.6)), onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) =>
                            NotificationsPage()
                    ),
                  );
                }),
              ],
            ),
            body: GestureDetector(
              onTap: () {
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              onVerticalDragDown: (DragDownDetails details){
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              onVerticalDragStart: (DragStartDetails details){
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              child:Container(
                width: width,
                height: height,
                color: Color(CustomColor.white),
                child: SingleChildScrollView(
                  child: Container(
                      width: width,
                      margin: EdgeInsets.only(top: 20),
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Column(
                        // mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          _active
                              ? _activeLoan() : _pending
                                ? _pendingSignature
                                  ? _pendingSignatureP() : _pendingLoan() : _declined
                                  ? _declinedLoan() : _approved
                                    ? _offerPending ? _pendingSignatureO() : _pendingLoan() : _noLoan(),
                          SizedBox(height: 20),
                          Card(
                            // elevation: 10.0,
                            shape: BeveledRectangleBorder(
                              borderRadius: BorderRadius.circular(5.0),
                            ),
                            child: Container(
                                decoration: BoxDecoration(
                                    gradient: LinearGradient(
                                        begin: Alignment.topRight,
                                        end: Alignment.bottomLeft,
                                        colors: [Color(CustomColor.offWhite), Color(CustomColor.white)]
                                    ),
                                  borderRadius: BorderRadius.circular(8.0),
                                ),
                              width: width,
                              padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                              child: Row(
                                children: [
                                  Image.asset("assets/images/support.png", width: 100),
                                  SizedBox(width: 10),
                                  Expanded(
                                    child: Container(
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Text("Have any queries, complaints, request or discussion you would like to bring to our attention?", textAlign: TextAlign.left, style:
                                          GoogleFonts.quicksand(
                                            textStyle: TextStyle(
                                              fontWeight: FontWeight.w500,
                                              fontSize: CustomSize.smallText,
                                              color: Color(CustomColor.black2),
                                            )
                                          )),
                                          TextButton(onPressed: (){
                                            Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (BuildContext context) =>
                                                      SupportPage()
                                              ),
                                            );
                                          }, child: Text("Contact Support", textAlign: TextAlign.center, style: GoogleFonts.quicksand(
                                            textStyle: TextStyle(
                                              fontWeight: FontWeight.w500,
                                              fontSize: CustomSize.normalText,
                                              color: Color(CustomColor.primary),
                                            )
                                          )),)
                                        ],
                                      ),
                                    ),
                                  )
                                ],
                              )
                            ),
                          )
                        ],
                      )
                  ),
                ),
              ),
            )
        )
    );
  }
}