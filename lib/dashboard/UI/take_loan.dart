import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:white_crust/data/custom_color.dart';
import 'package:flutter/cupertino.dart';
import 'loan_sub/custom.dart';
import 'loan_sub/lasg_federal.dart';
import 'loan_sub/npf.dart';
import 'loan_sub/nscdc.dart';
import 'package:white_crust/data/custom_sizes.dart';

class TakeLoan extends StatefulWidget {
  @override
  _TakeLoan createState() => _TakeLoan();
}

class _TakeLoan extends State<TakeLoan> {

  PageController controller;
  List<String> types = ['Nigeria Police Force', 'Custom Officers', 'Nigeria Security and Civil Defence Corps', 'Lagos State Government / Federal Workers'];

  Widget _action(String text, StatefulWidget widget){
    return GestureDetector(
      onTap: (){
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (BuildContext context) =>
                  widget
          ),
        );
      },
      child: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(vertical: 15, horizontal: 12),
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(color: Color(CustomColor.offBlack2).withOpacity(.5))
          ),
        ),
        child: Text(text, style: GoogleFonts.quicksand(
          textStyle: TextStyle(
            fontWeight: FontWeight.w500,
            fontSize: CustomSize.normalText,
            color: Color(CustomColor.black2),
          )
        )),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;
    return AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.dark,
        child: Scaffold(
            appBar: AppBar(
              backgroundColor: Color(CustomColor.white),
              elevation: 0.0,
              title: Text("Request ${Platform.isIOS ? 'an Offer':'a Loan'}", style: GoogleFonts.quicksand(
                textStyle: TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: CustomSize.bigText,
                  color: Color(CustomColor.black2),
                )
              )),
              iconTheme: IconThemeData(
                color: Color(CustomColor.primary), //change your color here
              ),
            ),
            body: GestureDetector(
              onTap: () {
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              onVerticalDragDown: (DragDownDetails details){
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              onVerticalDragStart: (DragStartDetails details){
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              child:Container(
                  width: width,
                  height: height,
                  color: Color(CustomColor.white),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      _action('Nigeria Police Force', NPF()),
                      _action('Custom Officers', CustomOfficer()),
                      _action('Nigeria Security and Civil Defence Corps', NSCDC()),
                      _action('Lagos State Government / Federal Workers', LASG()),
                    ],
                  )
              ),
            )
        )
    );
  }
  @override
  void initState() {
    super.initState();
    controller = new PageController();
  }


}