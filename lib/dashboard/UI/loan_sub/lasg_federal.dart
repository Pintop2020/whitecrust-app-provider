import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:white_crust/data/custom_color.dart';
import 'package:white_crust/data/custom_sizes.dart';
import 'package:white_crust/data/custom_strings.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/cupertino.dart';
import 'package:white_crust/data/login_session.dart';
import 'package:white_crust/data/utils.dart';
import 'package:international_phone_input/international_phone_input.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';
import 'dart:convert';
import 'package:dropdown_search/dropdown_search.dart';

class LASG extends StatefulWidget {
  @override
  _LASG createState() => _LASG();
}

class _LASG extends State<LASG> {

  double _progressWidth = 56;
  int slideIndex = 0;
  PageController controller;
  ApiBaseHelper apiBaseHelper = new ApiBaseHelper();
  var fullData;
  LoginSessionManager loginSessionManager = new LoginSessionManager();

  TextEditingController nameController = new TextEditingController(),
      homeController = new TextEditingController(),
      identityController = new TextEditingController(),
      nationalityController = new TextEditingController(),
      sooController = new TextEditingController(),
      lgaController = new TextEditingController(),
      emailController = new TextEditingController(),
      bvnController = new TextEditingController(),
      houseDescController = new TextEditingController(),
      maidensNameController = new TextEditingController(),
      institutionNameController = new TextEditingController(),
      officeAddressController = new TextEditingController(),
      lServiceController = new TextEditingController(),
      ippisController = new TextEditingController(),
      officialEmailAddressController = new TextEditingController(),
      annualIncomeController = new TextEditingController(),
      netMonthlyController = new TextEditingController(),
      accountController = new TextEditingController(),
      payDateController = new TextEditingController(),
      kinNameController = new TextEditingController(),
      kinAddressController = new TextEditingController(),
      amountRequestedController = new TextEditingController(),
      monthlyRepayController = new TextEditingController(),
      institutionController = new TextEditingController(),
      principalController = new TextEditingController(),
      outstandinglController = new TextEditingController(),
      otherRepaymentController = new TextEditingController();

  DateTime dob = DateTime.now(), dateEmployment = DateTime.now(), dateRetirement = DateTime.now(), dateObligation = DateTime.now();
  String _dob = '', _realDob = '', _dateEmployment = '', _realDateEmployment = '', _dateRetirement = '', _realDateRetirement = '';

  List<String> banks = [];
  List<Banks> _banks = [];
  Banks _sbankId;
  BanksFull dataF;
  var sGender = 'Male', sMeansOfId = 'NIN', sMaritalStatus = 'Single', sTenor = '3', sPayDate = '1', sBank = 'Zenith Bank', sRelationships = 'Brother';
  List<String> genders = ['Male', 'Female'];
  List<String> meansOfId = ['NIN', 'National ID Card', 'Passport'];
  List<String> maritalStatus = ['Single', 'Married', 'Widowed', 'Divorced', 'Seperated'];
  List<String> tenor = List<String>.generate(21, (int index) => '${index + 3}');
  List<String> payDate = List<String>.generate(30, (int index) => '${index+1}');
  List<String> relationships = ["Brother", "Sister", "Friend", "Parent", "Colleague", "Spouse"];
  String phoneNumber, phoneIsoCode = '+234';
  String kinPhoneNumber, kinPhoneIsoCode = '+234';
  File selectedPassport;
  final picker = ImagePicker();
  final Map<int, Widget> accountTypeWidget = const <int, Widget> {
    0: Text("Current Account"),
    1: Text("Savings Account"),
    2: Text("Others"),
  };
  var accountTypes = ['Current Account', 'Savings Account', 'Others'];
  var sAccountType = 'Current Account';
  int _sAccountType = 0;


  String nameErr, phoneErr, addressErr, identityErr, nationalityErr, stateErr, lgaErr, emailErr, bvnErr, hdescriptionErr, mothersNameErr, institutionErr, officeAddressErr,
      serviceLErr, ippisErr, officialEmailErr, annualIncomeErr, payingMsErr, netMonthIncomeErr, accountNoErr, kinNameErr, kinAddressErr, kinPhoneErr,
      amountErr, monthlyRepayErr;
  String dobErr, dateEmploymentErr, dateRetirementErr, dateObligationErr;


  void onPhoneNumberChange(String number, String internationalizedPhoneNumber, String isoCode) {
    phoneNumber = number;
    phoneIsoCode = isoCode;
    if(mounted) setState(() {});
  }

  void onPhoneNumberChangeKin(String number, String internationalizedPhoneNumber, String isoCode) {
    kinPhoneNumber = number;
    kinPhoneIsoCode = isoCode;
    if(mounted) setState(() {});
  }

  void _selectDateLater(String type) async {
    if(Platform.isAndroid){
      DateTime picked = await showDatePicker(
        context: context,
        initialDate: type == 'dob' ? dob : type == 'employment' ? dateEmployment : type == 'retirement' ? dateRetirement : dateObligation,
        firstDate: DateTime(1950),
        lastDate: DateTime(2050),
      );
      if(type == 'dob'){
        dob = picked;
        _dob = "${dob.day} ${monthNames[dob.month-1]}, ${dob.year}";
        _realDob = "${dob.year}-${dob.month}-${dob.day}";
      }else if(type == 'employment'){
        dateEmployment = picked;
        _dateEmployment = "${dateEmployment.day} ${monthNames[dateEmployment.month-1]}, ${dateEmployment.year}";
        _realDateEmployment = "${dateEmployment.year}-${dateEmployment.month}-${dateEmployment.day}";
      }else if(type == 'retirement'){
        dateRetirement = picked;
        _dateRetirement = "${dateRetirement.day} ${monthNames[dateRetirement.month-1]}, ${dateRetirement.year}";
        _realDateRetirement = "${dateRetirement.year}-${dateRetirement.month}-${dateRetirement.day}";
      }
      if(mounted) setState(() {});
    }else {
      showCupertinoModalPopup(
          context: context,
          builder: (context) {
            return Container(
                height: 350.0,
                color: Color(CustomColor.white),
                child: Column(
                  children: [
                    Container(
                      color: Color(CustomColor.offWhite),
                      child: Row(
                        children: [
                          TextButton(
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            child: Text(CustomStrings.cancel, textAlign: TextAlign.center,style: TextStyle(
                                fontWeight: FontWeight.w500,
                                fontSize: CustomSize.smallText,
                                color: Color(CustomColor.primary),
                                fontFamily: 'Poppins'
                            ),),
                          ),
                          Spacer(),
                          TextButton(
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            child: Text(CustomStrings.save, textAlign: TextAlign.center,style: TextStyle(
                                fontWeight: FontWeight.w500,
                                fontSize: CustomSize.smallText,
                                color: Color(CustomColor.success),
                                fontFamily: 'Poppins'
                            ),),
                          )
                        ],
                      ),
                    ),
                    Container(
                      height: 300,
                      child: CupertinoDatePicker(
                        maximumDate: DateTime(2050),
                        minuteInterval: 1,
                        initialDateTime: type == 'dob' ? dob : type == 'employment' ? dateEmployment : type == 'retirement' ? dateRetirement : dateObligation,
                        mode: CupertinoDatePickerMode.date,
                        onDateTimeChanged: (DateTime dateTime) {
                          if(type == 'dob'){
                            dob = dateTime;
                            _dob = "${dob.day} ${monthNames[dob.month-1]}, ${dob.year}";
                            _realDob = "${dob.year}-${dob.month}-${dob.day}";
                          }else if(type == 'employment'){
                            dateEmployment = dateTime;
                            _dateEmployment = "${dateEmployment.day} ${monthNames[dateEmployment.month-1]}, ${dateEmployment.year}";
                            _realDateEmployment = "${dateEmployment.year}-${dateEmployment.month}-${dateEmployment.day}";
                          }else if(type == 'retirement'){
                            dateRetirement = dateTime;
                            _dateRetirement = "${dateRetirement.day} ${monthNames[dateRetirement.month-1]}, ${dateRetirement.year}";
                            _realDateRetirement = "${dateRetirement.year}-${dateRetirement.month}-${dateRetirement.day}";
                          }
                          if(mounted) setState(() {});
                        },
                      ),
                    ),
                  ],
                )
            );
          });
    }
  }

  Widget _showErr(String error){
    return Text(error, style: TextStyle(
        fontWeight: FontWeight.w500,
        fontSize: CustomSize.smallText,
        color: Color(CustomColor.danger),
        fontFamily: 'Poppins'
    ));
  }

  Widget _input(TextEditingController controller, String label, {TextInputType type: TextInputType.text}) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          TextField(
            style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: CustomSize.normalText,
                color: Color(CustomColor.black2),
                fontFamily: 'Poppins'
            ),
            controller: controller,
            autocorrect: false,
            keyboardType: type,
            decoration: InputDecoration(
              hintText: label,
              hintStyle: TextStyle(
                  fontSize: CustomSize.normalText,
                  fontFamily: 'Poppins'
              ),
              filled: false,
              contentPadding: EdgeInsets.only(top: 10, bottom: 10, right: 10),
            ),
          )
        ],
      ),
    );
  }

  Widget _entryField(){
    final width = MediaQuery.of(context).size.width;
    return SingleChildScrollView(
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(height: 40),
              Text("APPLICANT's INFO", textAlign: TextAlign.center,style: TextStyle(
                  fontWeight: FontWeight.w800,
                  fontSize: CustomSize.bigTitle,
                  color: Color(CustomColor.offBlack),
                  fontFamily: 'Poppins'
              )),
              SizedBox(height: 70),
              _input(nameController, "Full Name"),
              if(nameErr != null) _showErr(nameErr),
              SizedBox(height: 10),
              GestureDetector(
                  onTap: (){
                    _selectDateLater('dob');
                  },
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Date of birth", style: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: CustomSize.buttonText,
                          color: Color(CustomColor.offBlack).withOpacity(.5),
                          fontFamily: 'Poppins'
                      )),
                      SizedBox(height: 10),
                      Container(
                          width: MediaQuery.of(context).size.width,
                          padding: EdgeInsets.symmetric(vertical: 15, horizontal: 10),
                          decoration: BoxDecoration(
                            color: Color(CustomColor.offBlack2).withOpacity(.2),
                            borderRadius: BorderRadius.circular(5),
                          ),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text(_dob, style: TextStyle(
                                  fontWeight: FontWeight.w500,
                                  fontSize: CustomSize.normalText,
                                  color: Color(CustomColor.black2),
                                  fontFamily: 'Poppins'
                              )),
                              Spacer(),
                              Icon(Icons.calendar_today_outlined, color: Color(CustomColor.primary), size: 20)
                            ],
                          )
                      )
                    ],
                  )
              ),
              if(dobErr != null) _showErr(dobErr),
              SizedBox(height: 10),
              Container(
                  width: width,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Gender",style: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: CustomSize.buttonText,
                          color: Color(CustomColor.primaryDark),
                          fontFamily: 'Poppins'
                      )),
                      SizedBox(height: 10),
                      Container(
                        width: width,
                        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                        decoration: BoxDecoration(
                          border: Border(
                              bottom: BorderSide(
                                color: Color(CustomColor.black2).withOpacity(.2), width: 1.0,
                              )
                          ),
                        ),
                        child: DropdownButton<String>(
                            isExpanded: true,
                            value: sGender,
                            icon: Icon(Icons.arrow_drop_down),
                            iconSize: 42,
                            underline: SizedBox(),
                            onChanged: (String newValue) {
                              setState(() {
                                sGender = newValue;
                              });
                            },
                            items: genders.map((String value) {
                              return new DropdownMenuItem<String>(
                                value: value,
                                child: new Text(value),
                              );
                            }).toList()
                        ),
                      )
                    ],
                  )
              ),
              SizedBox(height: 10),
              Container(
                decoration: BoxDecoration(
                    border: Border(
                        bottom: BorderSide(
                            color: Color(CustomColor.black2).withOpacity(.4)
                        )
                    )
                ),
                padding: EdgeInsets.symmetric(horizontal: 10),
                child: InternationalPhoneInput(
                  decoration: InputDecoration.collapsed(
                    hintText: 'Mobile number',
                    hintStyle: TextStyle(
                        fontSize: CustomSize.normalText,
                        fontFamily: 'Poppins'
                    ),
                    filled: false,
                  ),
                  onPhoneNumberChange: onPhoneNumberChange,
                  initialPhoneNumber: phoneNumber,
                  initialSelection: phoneIsoCode,
                  enabledCountries: ['+234'],
                  showCountryCodes: true,
                  showCountryFlags: true,
                ),
              ),
              if(phoneErr != null) _showErr(phoneErr),
              SizedBox(height: 10),
              _input(homeController, "Home Address"),
              if(addressErr != null) _showErr(addressErr),
              SizedBox(height: 10),
              Container(
                  width: width,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Means of Identification",style: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: CustomSize.buttonText,
                          color: Color(CustomColor.primaryDark),
                          fontFamily: 'Poppins'
                      )),
                      SizedBox(height: 10),
                      Container(
                        width: width,
                        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                        decoration: BoxDecoration(
                          border: Border(
                              bottom: BorderSide(
                                color: Color(CustomColor.black2).withOpacity(.2), width: 1.0,
                              )
                          ),
                        ),
                        child: DropdownButton<String>(
                            isExpanded: true,
                            value: sMeansOfId,
                            icon: Icon(Icons.arrow_drop_down),
                            iconSize: 42,
                            underline: SizedBox(),
                            onChanged: (String newValue) {
                              setState(() {
                                sMeansOfId = newValue;
                              });
                            },
                            items: meansOfId.map((String value) {
                              return new DropdownMenuItem<String>(
                                value: value,
                                child: new Text(value),
                              );
                            }).toList()
                        ),
                      )
                    ],
                  )
              ),
              SizedBox(height: 10),
              _input(identityController, "Identity Number / Serial Code"),
              if(identityErr != null) _showErr(identityErr),
              SizedBox(height: 10),
              Container(
                  width: width,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Marital Status",style: TextStyle(
                          fontWeight: FontWeight.w700,
                          fontSize: CustomSize.buttonText,
                          color: Color(CustomColor.primaryDark),
                          fontFamily: 'Roboto'
                      )),
                      SizedBox(height: 10),
                      Container(
                        width: width,
                        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                        decoration: BoxDecoration(
                          border: Border(
                              bottom: BorderSide(
                                color: Color(CustomColor.black2).withOpacity(.2), width: 1.0,
                              )
                          ),
                        ),
                        child: DropdownButton<String>(
                            isExpanded: true,
                            value: sMaritalStatus,
                            icon: Icon(Icons.arrow_drop_down),
                            iconSize: 42,
                            underline: SizedBox(),
                            onChanged: (String newValue) {
                              setState(() {
                                sMaritalStatus = newValue;
                              });
                            },
                            items: maritalStatus.map((String value) {
                              return new DropdownMenuItem<String>(
                                value: value,
                                child: new Text(value),
                              );
                            }).toList()
                        ),
                      )
                    ],
                  )
              ),
              SizedBox(height: 10),
              _input(nationalityController, "Nationality"),
              if(nationalityErr != null) _showErr(nationalityErr),
              SizedBox(height: 10),
              _input(sooController, "State of Origin"),
              if(stateErr != null) _showErr(stateErr),
              SizedBox(height: 10),
              _input(lgaController, "Local Government Area"),
              if(lgaErr != null) _showErr(lgaErr),
              SizedBox(height: 10),
              _input(emailController, "Personal Email", type: TextInputType.emailAddress),
              if(emailErr != null) _showErr(emailErr),
              SizedBox(height: 10),
              _input(bvnController, "Bank Verification Number (BVN)", type: TextInputType.number),
              if(bvnErr != null) _showErr(bvnErr),
              SizedBox(height: 10),
              _input(maidensNameController, "Mother's maiden name"),
              if(mothersNameErr != null) _showErr(mothersNameErr),
              SizedBox(height: 10),
              _input(houseDescController, "House Description", type: TextInputType.multiline),
              if(hdescriptionErr != null) _showErr(hdescriptionErr),
              SizedBox(height: 40),
              GestureDetector(
                onTap: validateOne,
                child: Container(
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5),
                        color: Color(CustomColor.primaryDark)
                    ),
                    padding: EdgeInsets.symmetric(vertical: 10, horizontal: 30),
                    child: Center(
                      child: Text("NEXT", style: TextStyle(
                          fontWeight: FontWeight.w600,
                          fontSize: CustomSize.buttonText,
                          color: Color(CustomColor.offWhite),
                          fontFamily: 'Poppins'
                      )),
                    )
                ),
              ),
              SizedBox(height: 40),
            ],
          ),
        )
    );
  }

  void validateOne(){
    nameErr = dobErr = phoneErr = addressErr = identityErr = nationalityErr = stateErr = lgaErr = emailErr = bvnErr = mothersNameErr = hdescriptionErr = null;
    if(nameController.text.isEmpty || nameController.text.split(' ').length < 2) nameErr = "Please enter your legal name";
    else if(_realDob.isEmpty || _realDob == '') dobErr = "Please select your legal date of birth";
    else if(phoneNumber.isEmpty || phoneNumber.length > 10 || phoneNumber.length < 10) phoneErr = "Please enter your valid phone number";
    else if(homeController.text.isEmpty) addressErr = "Please enter your home address";
    else if(identityController.text.isEmpty) identityErr = "Please enter your $sMeansOfId Code / Serial";
    else if(nationalityController.text.isEmpty) nationalityErr = "Please enter your nationality";
    else if(sooController.text.isEmpty) stateErr = "Please enter your state of origin";
    else if(lgaController.text.isEmpty) lgaErr = "Please enter your local government area";
    else if(!EmailValidator.validate(emailController.text)) emailErr = "Please enter a valid email address";
    else if(bvnController.text.isEmpty || bvnController.text.length < 11 || bvnController.text.length > 11) bvnErr = "Please enter your valid bank verification number";
    else if(maidensNameController.text.isEmpty) mothersNameErr = "Please enter your mother's maiden name";
    else if(houseDescController.text.isEmpty) hdescriptionErr = "Please guide us on how to navigate to your home address";
    else {
      nameErr = dobErr = phoneErr = addressErr = nationalityErr = stateErr = lgaErr = emailErr = bvnErr = mothersNameErr = hdescriptionErr = null;
      _progressWidth = _progressWidth+56;
      slideIndex = slideIndex+1;
    }
    if(nameErr != null ||  dobErr != null ||  phoneErr != null ||
        addressErr != null ||  identityErr != null ||  nationalityErr != null ||  stateErr != null ||
        lgaErr != null ||  emailErr != null ||  bvnErr != null || mothersNameErr != null ||
        hdescriptionErr != null) showInSnackBar(context, "Please fill all important fields.", Color(CustomColor.white), Color(CustomColor.danger));
    setState(() {
    });
    controller.animateToPage(slideIndex, duration: Duration(milliseconds: 500), curve: Curves.linear);
  }

  void selected(String value){
    _banks.forEach((element) {
      if(element.name == value){
        if(mounted)setState(() {
          _sbankId = element;
        });
      }
    });
    if(mounted)setState(() {
      sBank = value;
    });
  }

  Widget _entryField2(){
    final width = MediaQuery.of(context).size.width;
    return SingleChildScrollView(
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(height: 40),
              Text("EMPLOYMENT INFO", textAlign: TextAlign.center,style: TextStyle(
                  fontWeight: FontWeight.w800,
                  fontSize: CustomSize.bigTitle,
                  color: Color(CustomColor.offBlack),
                  fontFamily: 'Poppins'
              )),
              SizedBox(height: 70),
              _input(institutionNameController, "Name of Institution / Agency"),
              if(institutionErr != null) _showErr(institutionErr),
              SizedBox(height: 10),
              _input(officeAddressController, "Office Address"),
              if(officeAddressErr != null) _showErr(officeAddressErr),
              SizedBox(height: 10),
              _input(lServiceController, "Length Of Service"),
              if(serviceLErr != null) _showErr(serviceLErr),
              SizedBox(height: 10),
              _input(ippisController, "IPPIS Number"),
              if(ippisErr != null) _showErr(ippisErr),
              SizedBox(height: 10),
              _input(officialEmailAddressController, "Official Email Address"),
              if(officialEmailErr != null) _showErr(officialEmailErr),
              SizedBox(height: 10),
              GestureDetector(
                  onTap: (){
                    _selectDateLater('employment');
                  },
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Date of employment", style: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: CustomSize.buttonText,
                          color: Color(CustomColor.offBlack).withOpacity(.5),
                          fontFamily: 'Poppins'
                      )),
                      SizedBox(height: 10),
                      Container(
                          width: MediaQuery.of(context).size.width,
                          padding: EdgeInsets.symmetric(vertical: 15, horizontal: 10),
                          decoration: BoxDecoration(
                            color: Color(CustomColor.offBlack2).withOpacity(.2),
                            borderRadius: BorderRadius.circular(5),
                          ),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text(_dateEmployment, style: TextStyle(
                                  fontWeight: FontWeight.w500,
                                  fontSize: CustomSize.normalText,
                                  color: Color(CustomColor.black2),
                                  fontFamily: 'Poppins'
                              )),
                              Spacer(),
                              Icon(Icons.calendar_today_outlined, color: Color(CustomColor.primary), size: 20)
                            ],
                          )
                      )
                    ],
                  )
              ),
              SizedBox(height: 10),
              GestureDetector(
                  onTap: (){
                    _selectDateLater('retirement');
                  },
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Date of retirement", style: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: CustomSize.buttonText,
                          color: Color(CustomColor.offBlack).withOpacity(.5),
                          fontFamily: 'Poppins'
                      )),
                      SizedBox(height: 10),
                      Container(
                          width: MediaQuery.of(context).size.width,
                          padding: EdgeInsets.symmetric(vertical: 15, horizontal: 10),
                          decoration: BoxDecoration(
                            color: Color(CustomColor.offBlack2).withOpacity(.2),
                            borderRadius: BorderRadius.circular(5),
                          ),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Text(_dateRetirement, style: TextStyle(
                                  fontWeight: FontWeight.w500,
                                  fontSize: CustomSize.normalText,
                                  color: Color(CustomColor.black2),
                                  fontFamily: 'Poppins'
                              )),
                              Spacer(),
                              Icon(Icons.calendar_today_outlined, color: Color(CustomColor.primary), size: 20)
                            ],
                          )
                      )
                    ],
                  )
              ),
              SizedBox(height: 10),
              _input(annualIncomeController, "Annual Income", type: TextInputType.numberWithOptions(decimal: true)),
              if(annualIncomeErr != null) _showErr(annualIncomeErr),
              SizedBox(height: 10),
              _input(netMonthlyController, "Net Monthly Income", type: TextInputType.numberWithOptions(decimal: true)),
              if(netMonthIncomeErr != null) _showErr(netMonthIncomeErr),
              SizedBox(height: 10),
              Text("Bank", style: TextStyle(
                  fontSize: CustomSize.normalText,
                  color: Color(CustomColor.black2),
                  fontFamily: 'Poppins'
              )),
              SizedBox(height: 10),
              Container(
                width: width,
                padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Color(CustomColor.bgTwo)
                ),
                child: banks != null && banks.length > 0 ? DropdownSearch<String>(
                  mode: Mode.BOTTOM_SHEET,
                  showSelectedItem: true,
                  showAsSuffixIcons: true,
                  clearButtonBuilder: (_) => Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: const Icon(
                      Icons.clear,
                      size: 24,
                      color: Colors.black,
                    ),
                  ),
                  dropdownButtonBuilder: (_) => Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: const Icon(
                      Icons.keyboard_arrow_down_outlined,
                      size: 24,
                      color: Colors.black,
                    ),
                  ),
                  showSearchBox: true,
                  showClearButton: false,
                  dropdownSearchDecoration: InputDecoration(
                    labelStyle: TextStyle(
                        fontWeight: FontWeight.w900,
                        fontSize: CustomSize.normalText,
                        color: Color(CustomColor.offBlack),
                        fontFamily: 'Poppins'
                    ),
                    filled: false,
                    contentPadding: EdgeInsets.symmetric(horizontal: 10),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Color(CustomColor.bgTwo)),
                      borderRadius: BorderRadius.circular(5),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Color(CustomColor.bgTwo)),
                      borderRadius: BorderRadius.circular(5),
                    ),
                    border: OutlineInputBorder(
                      borderSide: BorderSide(color: Color(CustomColor.bgTwo)),
                      borderRadius: BorderRadius.circular(5),
                    ),
                  ),
                  items: banks.map((String e){
                    return e;
                  }).toList(),
                  label: "",
                  hint: "Select Bank",
                  selectedItem: sBank,
                  popupItemDisabled: (String s) => s.startsWith('I'),
                  onChanged: selected,
                ) : Row(
                  children: [
                    CircularProgressIndicator(),
                    SizedBox(width: 20),
                    Text("loading", style: TextStyle(
                        fontWeight: FontWeight.w500,
                        fontSize: CustomSize.normalText,
                        color: Color(CustomColor.offBlack),
                        fontFamily: 'Poppins'
                    )),
                  ],
                ),
              ),
              SizedBox(height: 10),
              _input(accountController, "Account Number", type: TextInputType.number),
              if(accountNoErr != null) _showErr(accountNoErr),
              SizedBox(height: 10),
              CupertinoSegmentedControl<int>(
                borderColor: Color(CustomColor.primary),
                selectedColor: Color(CustomColor.primary),
                children: accountTypeWidget,
                onValueChanged: (int index){
                  if(mounted)setState(() {
                    _sAccountType = index;
                    sAccountType = accountTypes[index];
                  });
                },
                groupValue: _sAccountType,
              ),
              SizedBox(height: 10),
              Container(
                  width: width,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Pay Date",style: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: CustomSize.buttonText,
                          color: Color(CustomColor.primaryDark),
                          fontFamily: 'Poppins'
                      )),
                      SizedBox(height: 10),
                      Container(
                        width: width,
                        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                        decoration: BoxDecoration(
                          border: Border(
                              bottom: BorderSide(
                                color: Color(CustomColor.black2).withOpacity(.2), width: 1.0,
                              )
                          ),
                        ),
                        child: DropdownButton<String>(
                            isExpanded: true,
                            value: sPayDate,
                            icon: Icon(Icons.arrow_drop_down),
                            iconSize: 42,
                            underline: SizedBox(),
                            onChanged: (String newValue) {
                              setState(() {
                                sPayDate = newValue;
                              });
                            },
                            items: payDate.map((String value) {
                              return new DropdownMenuItem<String>(
                                value: value,
                                child: new Text(value),
                              );
                            }).toList()
                        ),
                      )
                    ],
                  )
              ),
              SizedBox(height: 40),
              GestureDetector(
                onTap: validateTwo,
                child: Container(
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5),
                        color: Color(CustomColor.primaryDark)
                    ),
                    padding: EdgeInsets.symmetric(vertical: 10, horizontal: 30),
                    child: Center(
                      child: Text("NEXT", style: TextStyle(
                          fontWeight: FontWeight.w600,
                          fontSize: CustomSize.buttonText,
                          color: Color(CustomColor.offWhite),
                          fontFamily: 'Poppins'
                      )),
                    )
                ),
              ),
              SizedBox(height: 40),
            ],
          ),
        )
    );
  }

  void validateTwo(){
    institutionErr = officeAddressErr = serviceLErr = ippisErr = officialEmailErr = dateEmploymentErr = dateRetirementErr =
        annualIncomeErr = payingMsErr = netMonthIncomeErr = accountNoErr = null;
    if(institutionNameController.text.isEmpty) institutionErr = "Please enter your institution / agency name";
    else if(officeAddressController.text.isEmpty) officeAddressErr = "Please enter your office address";
    else if(lServiceController.text.isEmpty) serviceLErr = "Please enter your length of service";
    else if(ippisController.text.isEmpty) ippisErr = "Please enter your IPPIS number";
    else if(officialEmailAddressController.text.isEmpty) officialEmailErr = "Please enter your official email address";
    else if(_dateEmployment.isEmpty) dateEmploymentErr = "Please enter your employment date";
    else if(_dateRetirement.isEmpty) dateRetirementErr = "Please enter your retirement date";
    if(annualIncomeController.text.isEmpty) annualIncomeErr = "Please enter your annual income";
    else if(netMonthlyController.text.isEmpty) netMonthIncomeErr = "Please enter your net monthly income";
    else if(accountController.text.isEmpty) accountNoErr = "Please enter your account number";
    else {
      institutionErr = officeAddressErr = serviceLErr = ippisErr = officialEmailErr = dateEmploymentErr = dateRetirementErr =
          annualIncomeErr = payingMsErr = netMonthIncomeErr = accountNoErr = null;
      _progressWidth = _progressWidth+56;
      slideIndex = slideIndex+1;
    }
    if(institutionErr != null || officeAddressErr != null || serviceLErr != null || ippisErr != null || officialEmailErr != null || dateEmploymentErr != null || dateRetirementErr != null ||
        annualIncomeErr != null || payingMsErr != null || netMonthIncomeErr != null || accountNoErr != null)
      showInSnackBar(context, "Please fill all important fields.", Color(CustomColor.white), Color(CustomColor.danger));
    setState(() {
    });
    controller.animateToPage(slideIndex, duration: Duration(milliseconds: 500), curve: Curves.linear);
  }

  Widget _entryField3(){
    final width = MediaQuery.of(context).size.width;
    return SingleChildScrollView(
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(height: 40),
              Text("NEXT OF KIN INFO", textAlign: TextAlign.center,style: TextStyle(
                  fontWeight: FontWeight.w800,
                  fontSize: CustomSize.bigTitle,
                  color: Color(CustomColor.offBlack),
                  fontFamily: 'Poppins'
              )),
              SizedBox(height: 70),
              _input(kinNameController, "Name"),
              if(kinNameErr != null) _showErr(kinNameErr),
              SizedBox(height: 10),
              _input(kinAddressController, "Address"),
              if(kinAddressErr != null) _showErr(kinAddressErr),
              SizedBox(height: 10),
              Container(
                  width: width,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Relationship",style: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: CustomSize.buttonText,
                          color: Color(CustomColor.primaryDark),
                          fontFamily: 'Poppins'
                      )),
                      SizedBox(height: 10),
                      Container(
                        width: width,
                        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                        decoration: BoxDecoration(
                          border: Border(
                              bottom: BorderSide(
                                color: Color(CustomColor.black2).withOpacity(.2), width: 1.0,
                              )
                          ),
                        ),
                        child: DropdownButton<String>(
                            isExpanded: true,
                            value: sRelationships,
                            icon: Icon(Icons.arrow_drop_down),
                            iconSize: 42,
                            underline: SizedBox(),
                            onChanged: (String newValue) {
                              setState(() {
                                sRelationships = newValue;
                              });
                            },
                            items: relationships.map((String value) {
                              return new DropdownMenuItem<String>(
                                value: value,
                                child: new Text(value),
                              );
                            }).toList()
                        ),
                      )
                    ],
                  )
              ),
              SizedBox(height: 10),
              Container(
                decoration: BoxDecoration(
                    border: Border(
                        bottom: BorderSide(
                            color: Color(CustomColor.black2).withOpacity(.4)
                        )
                    )
                ),
                padding: EdgeInsets.symmetric(horizontal: 10),
                child: InternationalPhoneInput(
                  decoration: InputDecoration.collapsed(
                    hintText: 'Mobile number',
                    hintStyle: TextStyle(
                        fontSize: CustomSize.normalText,
                        fontFamily: 'Poppins'
                    ),
                    filled: false,
                  ),
                  onPhoneNumberChange: onPhoneNumberChangeKin,
                  initialPhoneNumber: kinPhoneNumber,
                  initialSelection: kinPhoneIsoCode,
                  enabledCountries: ['+234'],
                  showCountryCodes: true,
                  showCountryFlags: true,
                ),
              ),
              if(kinPhoneErr != null) _showErr(kinPhoneErr),
              SizedBox(height: 40),
              GestureDetector(
                onTap: (){
                  if(mounted)
                    setState(() {
                      _progressWidth = _progressWidth+40;
                      slideIndex = slideIndex+1;
                    });
                  controller.animateToPage(slideIndex, duration: Duration(milliseconds: 500), curve: Curves.linear);
                },
                child: Container(
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5),
                        color: Color(CustomColor.primaryDark)
                    ),
                    padding: EdgeInsets.symmetric(vertical: 10, horizontal: 30),
                    child: Center(
                      child: Text("NEXT", style: TextStyle(
                          fontWeight: FontWeight.w600,
                          fontSize: CustomSize.buttonText,
                          color: Color(CustomColor.offWhite),
                          fontFamily: 'Poppins'
                      )),
                    )
                ),
              ),
              SizedBox(height: 40),
            ],
          ),
        )
    );
  }

  void validateThree(){
    kinNameErr = kinAddressErr = kinPhoneErr = null;
    if(kinNameController.text.isEmpty) kinNameErr = "Please enter your next of kin's full legal name";
    else if(kinAddressController.text.isEmpty) kinAddressErr = "Please enter your next of kin's full address";
    else if(kinPhoneNumber.isEmpty || kinPhoneNumber.length < 10 || kinPhoneNumber.length > 10) kinPhoneErr = "Please enter your next of kin's valid phone number";
    else {
      kinNameErr = kinAddressErr = kinPhoneErr = null;
      _progressWidth = _progressWidth+56;
      slideIndex = slideIndex+1;
    }
    if(kinNameErr != null || kinAddressErr != null || kinPhoneErr != null)
      showInSnackBar(context, "Please fill all important fields.", Color(CustomColor.white), Color(CustomColor.danger));
    setState(() {
    });
    controller.animateToPage(slideIndex, duration: Duration(milliseconds: 500), curve: Curves.linear);
  }

  Widget _entryField4(){
    final width = MediaQuery.of(context).size.width;
    return SingleChildScrollView(
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(height: 40),
              Text("${Platform.isIOS ? 'REQUEST':'LOAN'} INFO", textAlign: TextAlign.center,style: TextStyle(
                  fontWeight: FontWeight.w800,
                  fontSize: CustomSize.bigTitle,
                  color: Color(CustomColor.offBlack),
                  fontFamily: 'Poppins'
              )),
              SizedBox(height: 70),
              _input(amountRequestedController, "Amount Requested", type: TextInputType.numberWithOptions(decimal: true)),
              if(amountErr != null) _showErr(amountErr),
              SizedBox(height: 10),
              Container(
                  width: width,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Tenor",style: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: CustomSize.buttonText,
                          color: Color(CustomColor.primaryDark),
                          fontFamily: 'Poppins'
                      )),
                      SizedBox(height: 10),
                      Container(
                        width: width,
                        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                        decoration: BoxDecoration(
                          border: Border(
                              bottom: BorderSide(
                                color: Color(CustomColor.black2).withOpacity(.2), width: 1.0,
                              )
                          ),
                        ),
                        child: DropdownButton<String>(
                            isExpanded: true,
                            value: sTenor,
                            icon: Icon(Icons.arrow_drop_down),
                            iconSize: 42,
                            underline: SizedBox(),
                            onChanged: (String newValue) {
                              setState(() {
                                sTenor = newValue;
                              });
                            },
                            items: tenor.map((String value) {
                              return new DropdownMenuItem<String>(
                                value: value,
                                child: new Text("$value months"),
                              );
                            }).toList()
                        ),
                      )
                    ],
                  )
              ),
              SizedBox(height: 10),
              _input(monthlyRepayController, "Affordable Monthly Repayment", type: TextInputType.numberWithOptions(decimal: true)),
              if(monthlyRepayErr != null) _showErr(monthlyRepayErr),
              SizedBox(height: 40),
              GestureDetector(
                onTap: validateFour,
                child: Container(
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5),
                        color: Color(CustomColor.primaryDark)
                    ),
                    padding: EdgeInsets.symmetric(vertical: 10, horizontal: 30),
                    child: Center(
                      child: Text("NEXT", style: TextStyle(
                          fontWeight: FontWeight.w600,
                          fontSize: CustomSize.buttonText,
                          color: Color(CustomColor.offWhite),
                          fontFamily: 'Poppins'
                      )),
                    )
                ),
              ),
              SizedBox(height: 40),
            ],
          ),
        )
    );
  }

  void validateFour(){
    amountErr = monthlyRepayErr = null;
    if(amountRequestedController.text.isEmpty
        || int.parse(amountRequestedController.text) < 50000 || int.parse(amountRequestedController.text) > 5000000) amountErr = "Enter amount of ${CustomStrings.currency}50,000 && "
        "maximum amount of ${CustomStrings.currency}5,000,000";
    else if(monthlyRepayController.text.isEmpty) monthlyRepayErr = "Please enter your monthly affordable repayment amount";
    else {
      amountErr = monthlyRepayErr = null;
      _progressWidth = _progressWidth+56;
      slideIndex = slideIndex+1;
    }
    if(amountErr != null || monthlyRepayErr != null)
      showInSnackBar(context, "Please fill all important fields.", Color(CustomColor.white), Color(CustomColor.danger));
    setState(() {
    });
    controller.animateToPage(slideIndex, duration: Duration(milliseconds: 500), curve: Curves.linear);
  }

  Widget _entryField5(){
    return SingleChildScrollView(
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(height: 40),
              Text("OTHER ${Platform.isIOS ? 'OFFERS':'LOANS'}/OBLIGATIONS", textAlign: TextAlign.center,style: TextStyle(
                  fontWeight: FontWeight.w800,
                  fontSize: CustomSize.bigTitle,
                  color: Color(CustomColor.offBlack),
                  fontFamily: 'Poppins'
              )),
              Text("Do you have an existing running ${Platform.isIOS ? 'offer':'loan'}. Please tell us about it, although we will run a check on credit "
                  "bureau to check how you are performing."
                  " Please note that this might affect the way your ${Platform.isIOS ? 'request':'loan'} is being processed. Please skip this form if you do "
                  "not have a running ${Platform.isIOS ? 'offer':'loan'}.", textAlign: TextAlign.center,style: TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: CustomSize.normalText,
                  color: Color(CustomColor.primary),
                  fontFamily: 'Poppins'
              )),
              SizedBox(height: 70),
              _input(institutionController, "Name of Institution"),
              SizedBox(height: 10),
              _input(principalController, "Amount", type: TextInputType.numberWithOptions(decimal: true)),
              SizedBox(height: 10),
              _input(outstandinglController, "Outstanding Balance", type: TextInputType.numberWithOptions(decimal: true)),
              SizedBox(height: 10),
              _input(otherRepaymentController, "Monthly Repayment", type: TextInputType.numberWithOptions(decimal: true)),
              SizedBox(height: 40),
              GestureDetector(
                onTap: validateFive,
                child: Container(
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5),
                        color: Color(CustomColor.primaryDark)
                    ),
                    padding: EdgeInsets.symmetric(vertical: 10, horizontal: 30),
                    child: Center(
                      child: Text("SUBMIT", style: TextStyle(
                          fontWeight: FontWeight.w600,
                          fontSize: CustomSize.buttonText,
                          color: Color(CustomColor.offWhite),
                          fontFamily: 'Poppins'
                      )),
                    )
                ),
              ),
              SizedBox(height: 40),
            ],
          ),
        )
    );
  }

  validateFive() async{
    var body = {
      "details":{
        "type":"lasg",
        "account_info":{
          "name":nameController.text,
          "dob":_realDob,
          "gender":sGender,
          "phone":phoneNumber,
          "address":homeController.text,
          "means_of_id":sMeansOfId,
          "identity_number":identityController.text,
          "marital_status":sMaritalStatus,
          "nationality":nationalityController.text,
          "state_of_origin":sooController.text,
          "local_government":lgaController.text,
          "personal_email":emailController.text,
          "bvn":bvnController.text,
          "house_description":houseDescController.text,
          "maiden_name":maidensNameController.text,
        },
        "employment_info":{
          "name_of_institution":institutionNameController.text,
          "office_address":officeAddressController.text,
          "length_of_service":lServiceController.text,
          "ippis_no":ippisController.text,
          "official_email":officialEmailAddressController.text,
          "date_of_employment":_realDateEmployment,
          "date_of_retirement":_realDateRetirement,
          "annual_income":annualIncomeController.text,
          "net_monthly_income":netMonthlyController.text,
          "bank": {
            'code': _sbankId.code,
            "name": _sbankId.name,
            "id": _sbankId.id
          },
          "account_number":accountController.text,
          "account_type":sAccountType,
          "pay_date":sPayDate
        },
        "next_kin_info":{
          "name":kinNameController.text,
          "address":kinAddressController.text,
          "relationship":sRelationships,
          "phone":kinPhoneNumber
        },
        "loan_info": {
          "amount":amountRequestedController.text,
          "tenor":sTenor,
          "monthly_repayment":monthlyRepayController.text
        },
        "other_obligations":{
          "name_of_institution":institutionController.text,
          "principal":principalController.text,
          "repayment":otherRepaymentController.text,
          "outstanding_amount":outstandinglController.text,
        }
      }
    };
    var response = await apiBaseHelper.post(context, UrlLinks.applyLoans, jsonEncode(body), await getHeader(), isLoader: true);
    if(response['status']){
      showSuccess(context, text: "Your application has been received.", actionText: "Close", onTap: (){
        Navigator.of(context).pop();
        Navigator.of(context).pop();
      });
    }else showInSnackBar(context, response['message'], Color(CustomColor.white), Color(CustomColor.danger));
  }


  Widget _progress(){
    return Container(
      width: _progressWidth,
      height: 10.0,
      decoration: BoxDecoration(
        color: Color(CustomColor.offWhite),
        borderRadius: BorderRadius.circular(90),
      ),
      child: Container(
        decoration: BoxDecoration(
          color: Color(CustomColor.primary),
          borderRadius: BorderRadius.circular(90),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;
    return AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.dark,
        child: Scaffold(
            appBar: AppBar(
              backgroundColor: Color(CustomColor.white),
              elevation: 0.0,
              title: _progress(),
              iconTheme: IconThemeData(
                color: Color(CustomColor.primary), //change your color here
              ),
              leading: slideIndex == 0 ? IconButton(
                icon: Icon(Icons.clear, color: Color(CustomColor.offBlack2)),
                onPressed: () => Navigator.of(context).pop(),
              ) : IconButton(
                icon: Icon(Icons.arrow_back, color: Color(CustomColor.offBlack2)),
                onPressed: () {
                  if(mounted)setState(() {
                    slideIndex = slideIndex-1;
                  });
                  controller.animateToPage(slideIndex, duration: Duration(milliseconds: 500), curve: Curves.linear);
                },
              ),
            ),
            body: GestureDetector(
              onTap: () {
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              onVerticalDragDown: (DragDownDetails details){
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              onVerticalDragStart: (DragStartDetails details){
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              child:Container(
                  width: width,
                  height: height,
                  color: Color(CustomColor.white),
                  child: Container(
                      width: width,
                      height: height-140,
                      //margin: EdgeInsets.only(bottom: 70),
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: PageView(
                        physics:new NeverScrollableScrollPhysics(),
                        controller: controller,
                        onPageChanged: (index) {
                          if(mounted)setState(() {
                            slideIndex = index;
                          });
                        },
                        children: <Widget>[
                          _entryField(),
                          _entryField2(),
                          _entryField3(),
                          _entryField4(),
                          _entryField5(),
                        ],
                      )
                  )
              ),
            )
        )
    );
  }

  loadBanks() async {
    var response = await apiBaseHelper.get(context, UrlLinks.banks, await getHeader());
    var encoded = jsonEncode(response['data']);
    if(mounted)setState(() {
      for(Map i in jsonDecode(encoded)){
        _banks.add(Banks.fromJson(i));
      }
      for(Map i in jsonDecode(encoded)){
        banks.add(i['name']);
      }
      if(dataF != null){
        _banks.forEach((element) {
          if(dataF.data.name == element.name){
            sBank = element.name;
            _sbankId = element;
          }
        });
      }else {
        sBank = _banks[0].name;
        _sbankId = _banks[0];
      }
    });
  }

  validateData() async {
    var response = await apiBaseHelper.get(context, UrlLinks.checkInterval, await getHeader(), isLoader: true);
    if(response['status']){
      _getData();
    }else showSuccess(context, isError: true, text: response['message'], actionText: "Close", onTap: (){
      Navigator.of(context).pop();
      Navigator.of(context).pop();
    });
  }

  _getData() async {
    var ddata = await loginSessionManager.getOtherData();
    fullData = ddata;
    if(ddata != null){
      var decoded = jsonDecode(ddata);
      var user = decoded['user'];
      nameController.text = "${user['first_name']} ${user['last_name']} ${user['other_name']}";
      emailController.text = user['email'];
      phoneNumber = '${user['mobile']}';
    }
    setState(() {
    });
    loadBanks();
  }

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () {
      validateData();
    });
    controller = new PageController();
  }


}