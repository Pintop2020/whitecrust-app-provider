import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:white_crust/data/custom_icons_icons.dart';
import 'package:white_crust/data/login_session.dart';
import 'package:white_crust/data/custom_color.dart';
import 'package:white_crust/data/custom_sizes.dart';
import 'package:white_crust/data/custom_strings.dart';
import 'package:flutter/cupertino.dart';
import 'package:white_crust/data/utils.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:intl/intl.dart';
import 'package:flutter_svg/flutter_svg.dart';

class SingleLoan extends StatefulWidget {
  SingleLoan({Key key, this.loan, this.user_id}) : super(key: key);
  final loan, user_id;
  @override
  _SingleLoan createState() => _SingleLoan();
}

class _SingleLoan extends State<SingleLoan> {
  final oCcy = new NumberFormat("#,##0.00", "en_US");
  var loan, decoded;
  ApiBaseHelper apiBaseHelper = new ApiBaseHelper();
  bool _sPage1 = false, _sPage2 = false, _sPage3 = false, _sPage4 = false, _sPage5 = false;

  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () {
      loadData();
    });
  }

  loadData() async {
    var response = await apiBaseHelper.get(context, UrlLinks.singleLoan+'/${widget.loan}', await getHeader(), isLoader: true);
    if(response['status']){
      loan = response['data'];
      decoded = jsonDecode(loan['details']);
    }else showInSnackBar(context, response['message'], Color(CustomColor.white), Color(CustomColor.danger));
    setState(() {
    });
  }

  Widget _action(String title, String body, {isBorder: true}){
    return Container(
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            color: Color(isBorder ? CustomColor.black2 : CustomColor.white).withOpacity(.5),
            width: .5
          )
        )
      ),
      padding: EdgeInsets.symmetric(vertical: 15, horizontal: 20),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
              child: Text(title, style: GoogleFonts.quicksand(
                textStyle: TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: CustomSize.normalText,
                  color: Color(CustomColor.black2),
                )
              ))),
          SizedBox(width: 20),
          Expanded(
            child: Text(body, textAlign: TextAlign.start, maxLines: 5, style: GoogleFonts.quicksand(
              textStyle: TextStyle(
                fontWeight: FontWeight.w700,
                fontSize: CustomSize.normalText,
                color: Color(CustomColor.black2),
              )
            ))
          )
        ],
      ),
    );
  }

  Widget _page1(){
    return Container(
      margin: EdgeInsets.zero,
      decoration: BoxDecoration(
        border: Border(
          left: BorderSide(
              color: Color(CustomColor.offBlack2),
              width: 1
          ),
          right: BorderSide(
              color: Color(CustomColor.offBlack2),
              width: 1
          ),
        ),
        color: Color(CustomColor.white),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          GestureDetector(
            onTap: (){
              _sPage1 = !_sPage1;
              _sPage2 = _sPage3 = _sPage4 = _sPage5 = false;
              setState(() {
              });
            },
            child: Container(
              decoration: BoxDecoration(
                border: Border(
                  top: BorderSide(
                      color: Color(CustomColor.offBlack2),
                      width: 1
                  ),
                  bottom: BorderSide(
                      color: Color(CustomColor.offBlack2),
                      width: 1
                  ),
                ),
                color: Color(CustomColor.offWhite),
              ),
              padding: EdgeInsets.symmetric(vertical: 15),
              child:Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text("Account Information", style: GoogleFonts.quicksand(
                    textStyle: TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: CustomSize.buttonText,
                      color: Color(CustomColor.black2),
                    )
                  )),
                  SizedBox(width: 10),
                  Icon(_sPage1 ? Icons.arrow_drop_up : Icons.arrow_drop_down, color: Color(CustomColor.black2), size: 15),
                ],
              ),
            )
          ),
          if(_sPage1)SizedBox(height: 20),
          Visibility(
            visible: _sPage1,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _action("Name", decoded['account_info']['name']),
                _action("Date of birth", decoded['account_info']['dob']),
                _action("Gender", decoded['account_info']['gender']),
                _action("Phone", decoded['account_info']['phone']),
                _action("Home Address", decoded['account_info']['address']),
                _action("Means Of ID", decoded['account_info']['means_of_id']),
                _action("ID Number", decoded['account_info']['identity_number']),
                _action("Marital Status", decoded['account_info']['marital_status']),
                _action("Nationality", decoded['account_info']['nationality']),
                _action("State Of Origin", decoded['account_info']['state_of_origin']),
                _action("Local Government Area", decoded['account_info']['local_government']),
                _action("Personal Email", decoded['account_info']['personal_email']),
                _action("BVN", decoded['account_info']['bvn']),
                _action("Mother's Maiden Name", decoded['account_info']['maiden_name'] ?? ''),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _page2(){
    return Container(
      margin: EdgeInsets.zero,
      decoration: BoxDecoration(
        border: Border(
          left: BorderSide(
              color: Color(CustomColor.offBlack2),
              width: 1
          ),
          right: BorderSide(
              color: Color(CustomColor.offBlack2),
              width: 1
          ),
        ),
        color: Color(CustomColor.white),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          GestureDetector(
              onTap: (){
                _sPage2 = !_sPage2;
                _sPage1 = _sPage3 = _sPage4 = _sPage5 = false;
                setState(() {
                });
              },
              child: Container(
                decoration: BoxDecoration(
                  border: Border(
                    top: BorderSide(
                        color: Color(CustomColor.offBlack2),
                        width: 1
                    ),
                    bottom: BorderSide(
                        color: Color(CustomColor.offBlack2),
                        width: 1
                    ),
                  ),
                  color: Color(CustomColor.offWhite),
                ),
                padding: EdgeInsets.symmetric(vertical: 15),
                child:Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text("Employment Information", style: GoogleFonts.quicksand(
                        textStyle: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: CustomSize.buttonText,
                          color: Color(CustomColor.black2),
                        )
                    )),
                    SizedBox(width: 10),
                    Icon(_sPage2 ? Icons.arrow_drop_up : Icons.arrow_drop_down, color: Color(CustomColor.black2), size: 15),
                  ],
                ),
              )
          ),
          if(_sPage2)SizedBox(height: 20),
          Visibility(
            visible: _sPage2,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _action("Work Division", decoded['employment_info']['work_division']),
                _action("Division Address", decoded['employment_info']['division_address']),
                _action("Length of Service", decoded['employment_info']['length_of_service']),
                _action("IPPIS No", decoded['employment_info']['ippis_no']),
                _action("Identity Number", decoded['employment_info']['id_number']),
                _action("Date Of Employment", decoded['employment_info']['date_of_employment']),
                _action("Date of Retirement", decoded['employment_info']['date_of_retirement']),
                _action("Rank", decoded['employment_info']['rank']),
                _action("Annual Income", decoded['employment_info']['annual_income']),
                _action("Paying MSS", decoded['employment_info']['paying_mss']),
                _action("Net Monthly Income", decoded['employment_info']['net_monthly_income']),
                _action("Bank (Salary Account)", decoded['employment_info']['bank']['name']),
                _action("Account Number", decoded['employment_info']['account_number']),
                _action("Account Type", decoded['employment_info']['account_type']),
                _action("Pay Date", decoded['employment_info']['pay_date']),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _page2Lasg(){
    return Container(
      margin: EdgeInsets.zero,
      decoration: BoxDecoration(
        border: Border(
          left: BorderSide(
              color: Color(CustomColor.offBlack2),
              width: 1
          ),
          right: BorderSide(
              color: Color(CustomColor.offBlack2),
              width: 1
          ),
        ),
        color: Color(CustomColor.white),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          GestureDetector(
              onTap: (){
                _sPage2 = !_sPage2;
                _sPage1 = _sPage3 = _sPage4 = _sPage5 = false;
                setState(() {
                });
              },
              child: Container(
                decoration: BoxDecoration(
                  border: Border(
                    top: BorderSide(
                        color: Color(CustomColor.offBlack2),
                        width: 1
                    ),
                    bottom: BorderSide(
                        color: Color(CustomColor.offBlack2),
                        width: 1
                    ),
                  ),
                  color: Color(CustomColor.offWhite),
                ),
                padding: EdgeInsets.symmetric(vertical: 15),
                child:Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text("Employment Information", style: GoogleFonts.quicksand(
                        textStyle: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: CustomSize.buttonText,
                          color: Color(CustomColor.black2),
                        )
                    )),
                    SizedBox(width: 10),
                    Icon(_sPage2 ? Icons.arrow_drop_up : Icons.arrow_drop_down, color: Color(CustomColor.black2), size: 15),
                  ],
                ),
              )
          ),
          if(_sPage2)SizedBox(height: 20),
          Visibility(
            visible: _sPage2,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _action("Name Of Institution / Agency", decoded['employment_info']['name_of_institution']),
                _action("Office Address", decoded['employment_info']['office_address']),
                _action("Length of Service", decoded['employment_info']['length_of_service']),
                _action("IPPIS No / Oracle No", decoded['employment_info']['ippis_no']),
                _action("Official Email", decoded['employment_info']['official_email']),
                _action("Date Of Employment", decoded['employment_info']['date_of_employment']),
                _action("Date of Retirement", decoded['employment_info']['date_of_retirement']),
                _action("Annual Income", decoded['employment_info']['annual_income']),
                _action("Net Monthly Income", decoded['employment_info']['net_monthly_income']),
                _action("Bank (Salary Account)", decoded['employment_info']['bank']['name']),
                _action("Account Number", decoded['employment_info']['account_number']),
                _action("Account Type", decoded['employment_info']['account_type']),
                _action("Pay Date", decoded['employment_info']['pay_date']),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _page2Custom(){
    return Container(
      margin: EdgeInsets.zero,
      decoration: BoxDecoration(
        border: Border(
          left: BorderSide(
              color: Color(CustomColor.offBlack2),
              width: 1
          ),
          right: BorderSide(
              color: Color(CustomColor.offBlack2),
              width: 1
          ),
        ),
        color: Color(CustomColor.white),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          GestureDetector(
              onTap: (){
                _sPage2 = !_sPage2;
                _sPage1 = _sPage3 = _sPage4 = _sPage5 = false;
                setState(() {
                });
              },
              child: Container(
                decoration: BoxDecoration(
                  border: Border(
                    top: BorderSide(
                        color: Color(CustomColor.offBlack2),
                        width: 1
                    ),
                    bottom: BorderSide(
                        color: Color(CustomColor.offBlack2),
                        width: 1
                    ),
                  ),
                  color: Color(CustomColor.offWhite),
                ),
                padding: EdgeInsets.symmetric(vertical: 15),
                child:Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text("Employment Information", style: GoogleFonts.quicksand(
                        textStyle: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: CustomSize.buttonText,
                          color: Color(CustomColor.black2),
                        )
                    )),
                    SizedBox(width: 10),
                    Icon(_sPage2 ? Icons.arrow_drop_up : Icons.arrow_drop_down, color: Color(CustomColor.black2), size: 15),
                  ],
                ),
              )
          ),
          if(_sPage2)SizedBox(height: 20),
          Visibility(
            visible: _sPage2,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _action("Work Division / Command", decoded['employment_info']['work_division']),
                _action("Work Division / Command Address", decoded['employment_info']['division_address']),
                _action("IPPIS No", decoded['employment_info']['ippis_no']),
                _action("Service No", decoded['employment_info']['service_no']),
                _action("File No", decoded['employment_info']['file_no']),
                _action("Rank", decoded['employment_info']['rank']),
                _action("Grade Level", decoded['employment_info']['grade_level']),
                _action("Step", decoded['employment_info']['step']),
                _action("Pay Point", decoded['employment_info']['pay_point']),
                _action("Date Of Employment", decoded['employment_info']['date_of_employment']),
                _action("Date of Retirement", decoded['employment_info']['date_of_retirement']),
                _action("Annual Income", decoded['employment_info']['annual_income']),
                _action("Net Monthly Income", decoded['employment_info']['net_monthly_income']),
                _action("Bank (Salary Account)", decoded['employment_info']['bank']['name']),
                _action("Account Number", decoded['employment_info']['account_number']),
                _action("Account Type", decoded['employment_info']['account_type']),
                _action("Pay Date", decoded['employment_info']['pay_date']),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _page3(){
    return Container(
      margin: EdgeInsets.zero,
      decoration: BoxDecoration(
        border: Border(
          left: BorderSide(
              color: Color(CustomColor.offBlack2),
              width: 1
          ),
          right: BorderSide(
              color: Color(CustomColor.offBlack2),
              width: 1
          ),
        ),
        color: Color(CustomColor.white),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          GestureDetector(
              onTap: (){
                _sPage3 = !_sPage3;
                _sPage1 = _sPage2 = _sPage4 = _sPage5 = false;
                setState(() {
                });
              },
              child: Container(
                decoration: BoxDecoration(
                  border: Border(
                    top: BorderSide(
                        color: Color(CustomColor.offBlack2),
                        width: 1
                    ),
                    bottom: BorderSide(
                        color: Color(CustomColor.offBlack2),
                        width: 1
                    ),
                  ),
                  color: Color(CustomColor.offWhite),
                ),
                padding: EdgeInsets.symmetric(vertical: 15),
                child:Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text("Next Of Kin", style: GoogleFonts.quicksand(
                        textStyle: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: CustomSize.buttonText,
                          color: Color(CustomColor.black2),
                        )
                    )),
                    SizedBox(width: 10),
                    Icon(_sPage3 ? Icons.arrow_drop_up : Icons.arrow_drop_down, color: Color(CustomColor.black2), size: 15),
                  ],
                ),
              )
          ),
          if(_sPage3)SizedBox(height: 20),
          Visibility(
            visible: _sPage3,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _action("Name", decoded['next_kin_info']['name']),
                _action("Phone", decoded['next_kin_info']['phone']),
                _action("Address", decoded['next_kin_info']['address']),
                _action("Relationship", decoded['next_kin_info']['relationship']),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _page4(){
    return Container(
      margin: EdgeInsets.zero,
      decoration: BoxDecoration(
        border: Border(
          left: BorderSide(
              color: Color(CustomColor.offBlack2),
              width: 1
          ),
          right: BorderSide(
              color: Color(CustomColor.offBlack2),
              width: 1
          ),
        ),
        color: Color(CustomColor.white),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          GestureDetector(
              onTap: (){
                _sPage4 = !_sPage4;
                _sPage1 = _sPage2 = _sPage3 = _sPage5 = false;
                setState(() {
                });
              },
              child: Container(
                decoration: BoxDecoration(
                  border: Border(
                    top: BorderSide(
                        color: Color(CustomColor.offBlack2),
                        width: 1
                    ),
                    bottom: BorderSide(
                        color: Color(CustomColor.offBlack2),
                        width: 1
                    ),
                  ),
                  color: Color(CustomColor.offWhite),
                ),
                padding: EdgeInsets.symmetric(vertical: 15),
                child:Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text("${Platform.isIOS ? 'Offer':'Loan'} Details", style: GoogleFonts.quicksand(
                        textStyle: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: CustomSize.buttonText,
                          color: Color(CustomColor.black2),
                        )
                    )),
                    SizedBox(width: 10),
                    Icon(_sPage4 ? Icons.arrow_drop_up : Icons.arrow_drop_down, color: Color(CustomColor.black2), size: 15),
                  ],
                ),
              )
          ),
          if(_sPage4)SizedBox(height: 20),
          Visibility(
            visible: _sPage4,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _action("Amount Requested", "N${oCcy.format(double.parse(decoded['loan_info']['amount']))}"),
                _action("Tenor", "${decoded['loan_info']['tenor']} months"),
                _action("Affordable Monthly Repayment", "N${oCcy.format(double.parse(decoded['loan_info']['monthly_repayment']))}"),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _page5(){
    return Container(
      margin: EdgeInsets.zero,
      decoration: BoxDecoration(
        border: Border(
          left: BorderSide(
              color: Color(CustomColor.offBlack2),
              width: 1
          ),
          right: BorderSide(
              color: Color(CustomColor.offBlack2),
              width: 1
          ),
        ),
        color: Color(CustomColor.white),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          GestureDetector(
              onTap: (){
                _sPage5 = !_sPage5;
                _sPage1 = _sPage2 = _sPage3 = _sPage4 = false;
                setState(() {
                });
              },
              child: Container(
                decoration: BoxDecoration(
                  border: Border(
                    top: BorderSide(
                        color: Color(CustomColor.offBlack2),
                        width: 1
                    ),
                    bottom: BorderSide(
                        color: Color(CustomColor.offBlack2),
                        width: 1
                    ),
                  ),
                  color: Color(CustomColor.offWhite),
                ),
                padding: EdgeInsets.symmetric(vertical: 15),
                child:Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text("Other ${Platform.isIOS ? 'Offer':'Loan'} / Obligations", style: GoogleFonts.quicksand(
                        textStyle: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: CustomSize.buttonText,
                          color: Color(CustomColor.black2),
                        )
                    )),
                    SizedBox(width: 10),
                    Icon(_sPage5 ? Icons.arrow_drop_up : Icons.arrow_drop_down, color: Color(CustomColor.black2), size: 15),
                  ],
                ),
              )
          ),
          if(_sPage5)SizedBox(height: 20),
          if(decoded['other_obligations']['name_of_institution'] != null)Visibility(
            visible: _sPage5,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _action("Name of Institution", decoded['other_obligations']['name_of_institution']),
                _action("Principal", "N${oCcy.format(double.parse(decoded['other_obligations']['principal']))}"),
                _action("Monthly Repayment", "N${oCcy.format(double.parse(decoded['other_obligations']['repayment']))}"),
                _action("Balance", "N${oCcy.format(double.parse(decoded['other_obligations']['outstanding_amount']))}"),
              ],
            ),
          )
        ],
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;
    return AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.dark,
        child: Scaffold(
            appBar: AppBar(
              backgroundColor: Color(CustomColor.white),
              title: Text("My ${Platform.isIOS ? 'Request':'Loan'}", style: GoogleFonts.quicksand(
                textStyle: TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: CustomSize.bigText,
                  color: Color(CustomColor.primary),
                )
              )),
              iconTheme: IconThemeData(
                color: Color(CustomColor.offBlack), //change your color here
              ),
            ),
            body: GestureDetector(
              onTap: () {
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              onVerticalDragDown: (DragDownDetails details){
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              onVerticalDragStart: (DragStartDetails details){
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              child:Container(
                width: width,
                height: height,
                color: Color(CustomColor.white),
                child: SingleChildScrollView(
                  child: Container(
                      width: width,
                      child: loan != null ? Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(height: 20),
                          Center(
                            child: Container(
                              width: 100,
                              height: 100,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                border: Border.all(color: Color(CustomColor.primaryDark),width: 2),
                                image: new DecorationImage(
                                  image: CachedNetworkImageProvider(UrlLinks.mediaSite+'/'+decoded['account_info']['passport']),
                                  fit: BoxFit.cover,
                                ),
                              ),
                            )
                          ),
                          SizedBox(height: 10),
                          Center(child: status(loan)),
                          SizedBox(height: 10),
                          GestureDetector(
                              onTap: (){
                                launchURL("${UrlLinks.mainSite}loans/preview/${loan['uuid']}");
                              },
                              child: Container(
                                padding: EdgeInsets.symmetric(vertical: 15, horizontal: 20),
                                decoration: BoxDecoration(
                                  color: Color(CustomColor.primary),
                                ),
                                child: Center(
                                  child: Text("Download Form", style: GoogleFonts.quicksand(
                                    textStyle: TextStyle(
                                      fontWeight: FontWeight.w600,
                                      fontSize: CustomSize.buttonText,
                                      color: Color(CustomColor.white),
                                    )
                                  )),
                                ),
                              )
                          ),
                          if(loan['signatures'].toList().length < 1) GestureDetector(
                              onTap: (){
                                launchURL("${loan['signature_link']}/${widget.user_id}");
                              },
                              child: Container(
                                padding: EdgeInsets.symmetric(vertical: 15, horizontal: 20),
                                decoration: BoxDecoration(
                                  color: Color(CustomColor.accent),
                                ),
                                child: Center(
                                  child: Text("Sign Application Form", style: GoogleFonts.quicksand(
                                    textStyle: TextStyle(
                                      fontWeight: FontWeight.w600,
                                      fontSize: CustomSize.buttonText,
                                      color: Color(CustomColor.white),
                                    )
                                  )),
                                ),
                              )
                          ),
                          if(loan['offer'] != null) GestureDetector(
                              onTap: (){
                                launchURL("${UrlLinks.mainSite}offers/preview/${loan['offer']['uuid']}");
                              },
                              child: Container(
                                padding: EdgeInsets.symmetric(vertical: 15, horizontal: 20),
                                decoration: BoxDecoration(
                                  color: Color(CustomColor.mediumSeaGreen),
                                ),
                                child: Center(
                                  child: Text("Download Offer Letter", style: GoogleFonts.quicksand(
                                    textStyle: TextStyle(
                                      fontWeight: FontWeight.w600,
                                      fontSize: CustomSize.buttonText,
                                      color: Color(CustomColor.white),
                                    )
                                  )),
                                ),
                              )
                          ),
                          if(loan['offer'] != null && loan['offer']['signature'] == null) GestureDetector(
                              onTap: (){
                                launchURL("${loan['offer']['signature_link']}/${widget.user_id}");
                              },
                              child: Container(
                                padding: EdgeInsets.symmetric(vertical: 15, horizontal: 20),
                                decoration: BoxDecoration(
                                  color: Color(CustomColor.seaTurtle),
                                ),
                                child: Center(
                                  child: Text("Sign Offer Letter", style: GoogleFonts.quicksand(
                                    textStyle: TextStyle(
                                      fontWeight: FontWeight.w600,
                                      fontSize: CustomSize.buttonText,
                                      color: Color(CustomColor.white),
                                    )
                                  )),
                                ),
                              )
                          ),
                          Center(
                            child: Text(getTimeConverted2(loan['created_at']), style: GoogleFonts.quicksand(
                              textStyle: TextStyle(
                                fontWeight: FontWeight.w600,
                                fontSize: CustomSize.buttonText,
                                color: Color(CustomColor.seaTurtle),
                              )
                            ))
                          ),
                          _page1(),
                          decoded['type'] == 'npf' ? _page2() : decoded['type'] == 'lasg' ? _page2Lasg() : _page2Custom(),
                          _page3(),
                          _page4(),
                          _page5(),
                          SizedBox(height: 20),
                          if(loan != null && loan['signatures'].toList().length > 0)
                            Center(child: SvgPicture.string(jsonDecode(loan['signatures'][0]['details'])['signature'])),
                          if(loan != null && loan['signatures'].toList().length > 0)
                            Center(child: Text(getTimeConverted2(loan['signatures'][0]['created_at']), style: GoogleFonts.quicksand(
                              textStyle: TextStyle(
                                fontWeight: FontWeight.w600,
                                fontSize: CustomSize.buttonText,
                                color: Color(CustomColor.seaTurtle),
                              )
                            ))),
                          SizedBox(height: 50),
                        ],
                      ) : Container()
                  ),
                ),
              ),
            )
        )
    );
  }
}