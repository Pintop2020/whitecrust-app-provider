import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_tawk/flutter_tawk.dart';
import 'package:white_crust/data/custom_color.dart';
import 'package:white_crust/data/custom_sizes.dart';
import 'package:white_crust/data/custom_strings.dart';
import 'package:white_crust/data/login_session.dart';
import 'package:white_crust/data/utils.dart';

class SupportPage extends StatefulWidget {
  @override
  _SupportPage createState() => _SupportPage();
}

class _SupportPage extends State<SupportPage> {

  LoginSessionManager loginSessionManager = new LoginSessionManager();
  var name = '', email = '';

  _getData() async {
    var data = jsonDecode(await loginSessionManager.getOtherData());
    name = data['user']['first_name']+' '+data['user']['last_name'];
    email = data['user']['email'];
    if(mounted)setState(() {
    });
  }


  @override
  void initState() {
    super.initState();
    _getData();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        appBar: AppBar(
          title: Text("SUPPORT DESK", textAlign: TextAlign.center,style: TextStyle(
              fontWeight: FontWeight.w600,
              fontSize: CustomSize.normalText,
              color: Color(CustomColor.black2),
              fontFamily: 'Poppins'
          )),
          backgroundColor: Color(CustomColor.white),
          elevation: 0.0,
          iconTheme: IconThemeData(
            color: Color(CustomColor.black2), //change your color here
          ),
        ),
        body: Tawk(
          directChatLink: CustomStrings.tawk_direct_chat_link,
          visitor: TawkVisitor(
            name: name,
            email: email,
          ),
          onLoad: () {
            print('Hello!');
          },
          onLinkTap: (String url) {
            launchURL(url);
          },
          placeholder: Center(
            child: Text('Loading...'),
          ),
        ),
      ),
    );
  }
}