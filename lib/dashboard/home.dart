import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:google_fonts/google_fonts.dart';
import '../data/custom_icons_icons.dart';
import '../data/login_session.dart';
import '../data/utils.dart';
import 'package:white_crust/dashboard/UI/home.dart';
import 'package:white_crust/dashboard/UI/loans.dart';
import 'package:white_crust/dashboard/UI/profile.dart';
import 'package:white_crust/data/custom_color.dart';
import 'package:white_crust/data/custom_sizes.dart';

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  print("Handling a background message: ${message.messageId}");
}

foreground() async {
  await FirebaseMessaging.instance.setForegroundNotificationPresentationOptions(
    alert: true,
    badge: true,
    sound: true,
  );
}

class App extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => AppState();
}

class AppState extends State<App> {
  final FirebaseMessaging _messaging = FirebaseMessaging.instance;
  PushNotification _notificationInfo = PushNotification();
  int currentTab = 0;
  LoginSessionManager loginSessionManager = new LoginSessionManager();
  ApiBaseHelper apiBaseHelper = new ApiBaseHelper();
  Timer timer;
  PageController controller;

  void registerNotification() async {
    await Firebase.initializeApp();

    NotificationSettings settings = await _messaging.requestPermission(
      alert: true,
      badge: true,
      provisional: false,
      sound: true,
    );

    if (settings.authorizationStatus == AuthorizationStatus.authorized) {
      print('User granted permission');
      FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);
      // For handling the received notifications
      FirebaseMessaging.onMessage.listen((RemoteMessage message) {
        // Parse the message received
        PushNotification notification = PushNotification(
          title: message.notification?.title,
          body: message.notification?.body,
        );
        if (_notificationInfo != null) {
          // For displaying the notification as an overlay
          // showSimpleNotification(
          //   Text(_notificationInfo.title),
          //   leading: NotificationBadge(totalNotifications: _totalNotifications),
          //   subtitle: Text(_notificationInfo!.body!),
          //   background: Colors.cyan.shade700,
          //   duration: Duration(seconds: 2),
          // );
        }
        setState(() {
          _notificationInfo = notification;
        });
      });

      FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
        PushNotification notification = PushNotification(
          title: message.notification?.title,
          body: message.notification?.body,
        );
        setState(() {
          _notificationInfo = notification;
          // _totalNotifications++;
        });
      });
    } else {
      // print('User declined or has not accepted permission');
    }
  }

  Future<void> setupInteractedMessage() async {
    // Get any messages which caused the application to open from
    // a terminated state.
    RemoteMessage initialMessage =
    await FirebaseMessaging.instance.getInitialMessage();

    // If the message also contains a data property with a "type" of "chat",
    // navigate to a chat screen
    // if (initialMessage != null && initialMessage.data['type'] == 'chat') {
    //   Navigator.pushNamed(context, '/chat',
    //       arguments: ChatArguments(initialMessage));
    // }

    // Also handle any interaction when the app is in the background via a
    // Stream listener
    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      // if (message.data['type'] == 'chat') {
      //   Navigator.pushNamed(context, '/chat',
      //       arguments: ChatArguments(message));
      // }
      // PushNotification notification = PushNotification(
      //   title: message.notification?.title,
      //   body: message.notification?.body,
      //   // dataTitle: message.data['title'],
      //   // dataBody: message.data['body'],
      // );
      // setState(() {
      //   // _notificationInfo = notification;
      //   // _totalNotifications++;
      // });
    });
  }

  validateData() async {
    var decoded = await apiBaseHelper.get(context, UrlLinks.getUser, await getHeader());
    if(decoded['status']) {
      var user = jsonEncode(decoded['data']);
      loginSessionManager.setOtherData(user);
    }
  }

  @override
  void initState() {
    super.initState();
    controller = new PageController(initialPage: currentTab);
    timer = Timer.periodic(Duration(seconds: 20), (Timer t) => validateData());
    foreground();
    registerNotification();
    setupInteractedMessage();
  }

  @override
  void dispose() {
    timer?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        bottomNavigationBar: BottomNavigationBar(
          backgroundColor: Color(CustomColor.offWhite),
          unselectedItemColor: Color(CustomColor.black2).withOpacity(.4),
          type: BottomNavigationBarType.fixed,
          onTap: (value){
            setState(() {
              currentTab = value;
            });
            controller.animateToPage(currentTab, duration: Duration(milliseconds: 500), curve: Curves.linear);
          },
          currentIndex: currentTab,
          selectedLabelStyle: GoogleFonts.quicksand(
            textStyle: TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: CustomSize.smallText,
            )
          ),
          unselectedLabelStyle: GoogleFonts.quicksand(
              textStyle: TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: CustomSize.smallText,
              )
          ),
          items: [
            BottomNavigationBarItem(
              icon: new Icon(CustomIcons.home_run),
              label: "Dashboard",
            ),
            BottomNavigationBarItem(
              icon: Icon(CustomIcons.donation),
              label: "${Platform.isIOS ? 'Requests':'Loans'}",
            ),
            BottomNavigationBarItem(
              icon: Icon(CustomIcons.person),
              label: "Profile",
            ),
          ],
        ),
        body: PageView(
          physics: NeverScrollableScrollPhysics(),
          controller: controller,
          onPageChanged: (index) {
            setState(() {
              currentTab = index;
            });
          },
          children: [
            Dashboard(),
            LoanHistory(),
            ProfilePage(),
          ],
        )
    );
  }
}