import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:white_crust/dashboard/home.dart';
import 'package:white_crust/data/login_session.dart';
import 'data/custom_color.dart';
import 'data/utils.dart';
import 'home.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  final splashDelay = 5;
  LoginSessionManager loginSessionManager = new LoginSessionManager();
  ApiBaseHelper _helper = new ApiBaseHelper();

  @override
  void initState() {
    super.initState();
    _loadWidget();
  }

  _loadWidget() async {
    var _duration = Duration(seconds: splashDelay);
    return Timer(_duration, fullLoader);
  }

  void fullLoader() async{
    if(await loginSessionManager.isUserLogged()){
      _validateData();
    }else {
      Navigator.pushAndRemoveUntil(context, MaterialPageRoute(
        builder: (BuildContext context) => Home(),
      ), (r) => false);
    }
  }

  _validateData() async {
    var decoded = await _helper.get(context, UrlLinks.getUser, await getHeader());
    if(!decoded['status']){
      if(decoded['message'] == 'Unauthenticated'){
        loginSessionManager.logout();
        Navigator.pushAndRemoveUntil(context, MaterialPageRoute(
          builder: (BuildContext context) => Home(),
        ), (r) => false);
      }else {
        showInSnackBar(context, decoded['message'], Color(CustomColor.accent), Color(CustomColor.bgTwo));
      }
    }else if(decoded['status']) {
      var user = jsonEncode(decoded['data']);
      loginSessionManager.setOtherData(user);
      loginSessionManager.logUserIn();
      navigationPage();
    }
  }

  void navigationPage() async {
    if(await loginSessionManager.isUserLogged()){
      Navigator.pushAndRemoveUntil(context, MaterialPageRoute(
        builder: (BuildContext context) => App(),
      ), (r) => false);
    }else {
      Navigator.pushAndRemoveUntil(context, MaterialPageRoute(
        builder: (BuildContext context) => Home(),
      ), (r) => false);
    }
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;
    return Scaffold(
      body: Container(
        child: Stack(
          children: [
            Container(
                width: width,
                height: height,
                color: Color(CustomColor.primaryDark),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    // Text("Welcome to", style: TextStyle(
                    //     fontWeight: FontWeight.w500,
                    //     fontSize: CustomSize.giantText,
                    //     color: Color(CustomColor.offWhite),
                    //     fontFamily: 'Poppins'
                    // )),
                    // Text("Seal-Sea Concepts", style: TextStyle(
                    //     fontWeight: FontWeight.w500,
                    //     fontSize: CustomSize.bigTitle,
                    //     color: Color(CustomColor.primary),
                    //     fontFamily: 'Poppins'
                    // )),
                    Image.asset("assets/images/logo2.png", width: 200),
                  ],
                )
            ),
          ],
        ),
      ),
    );
  }
}