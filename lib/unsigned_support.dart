import 'package:flutter/material.dart';
import 'package:flutter_tawk/flutter_tawk.dart';
import 'package:white_crust/data/custom_color.dart';
import 'package:white_crust/data/custom_sizes.dart';
import 'package:white_crust/data/custom_strings.dart';
import 'package:white_crust/data/utils.dart';

class SupportPageMain extends StatefulWidget {
  @override
  _SupportPageMain createState() => _SupportPageMain();
}

class _SupportPageMain extends State<SupportPageMain> {

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        appBar: AppBar(
          title: Text("HELP DESK", textAlign: TextAlign.center,style: TextStyle(
              fontWeight: FontWeight.w600,
              fontSize: CustomSize.normalText,
              color: Color(CustomColor.black2),
              fontFamily: 'Poppins'
          )),
          automaticallyImplyLeading: true,
          backgroundColor: Color(CustomColor.white),
          iconTheme: IconThemeData(
            color: Color(CustomColor.black2), //change your color here
          ),
        ),
        body: Tawk(
          directChatLink: CustomStrings.tawk_direct_chat_link,
          visitor: TawkVisitor(
            name: '',
            email: '',
          ),
          onLoad: () {
            print('Hello!');
          },
          onLinkTap: (String url) {
            launchURL(url);
          },
          placeholder: Center(
            child: Text('Loading support desk..'),
          ),
        ),
      ),
    );
  }
}