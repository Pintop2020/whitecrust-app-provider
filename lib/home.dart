import 'package:google_fonts/google_fonts.dart';
import 'package:white_crust/auth/register.dart';
import 'package:white_crust/unsigned_support.dart';
import 'slider_data.dart';
import 'package:flutter/material.dart';
import 'data/custom_strings.dart';
import 'data/custom_color.dart';
import 'data/custom_sizes.dart';
import 'auth/login.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

  List<SliderModel> mySLides = [];
  int slideIndex = 0;
  PageController controller;

  Widget _buildPageIndicator(bool isCurrentPage){
    return isCurrentPage ? Container(
      margin: EdgeInsets.symmetric(horizontal: 2.0),
      height: 10.0,
      width: 10.0,
      decoration: BoxDecoration(
        color: Color(CustomColor.primary),
        borderRadius: BorderRadius.circular(12),
      ),
    )  : Container(
      margin: EdgeInsets.symmetric(horizontal: 2.0),
      height: 10.0,
      width: 10.0,
      decoration: BoxDecoration(
        color: Color(CustomColor.white),
        border: Border.all(color: Color(CustomColor.primary), width: 1.0),
        borderRadius: BorderRadius.circular(12),
      ),
    );
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    mySLides = getSlides();
    controller = new PageController();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: Container(
            height: MediaQuery.of(context).size.height,
            child: Stack(
              children: [
                PageView(
                  controller: controller,
                  onPageChanged: (index) {
                    setState(() {
                      slideIndex = index;
                    });
                  },
                  children: <Widget>[
                    SlideTile(
                      imagePath: mySLides[0].getImageAssetPath(),
                      title: mySLides[0].getTitle(),
                      desc: mySLides[0].getDesc(),
                    ),
                    SlideTile(
                      imagePath: mySLides[1].getImageAssetPath(),
                      title: mySLides[1].getTitle(),
                      desc: mySLides[1].getDesc(),
                    ),
                    SlideTile(
                      imagePath: mySLides[2].getImageAssetPath(),
                      title: mySLides[2].getTitle(),
                      desc: mySLides[2].getDesc(),
                    )
                  ],
                ),
                Positioned(
                    bottom: 0,
                    left: 7,
                    right: 7,
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      margin: EdgeInsets.only(bottom: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            width: 70,
                            child: Row(
                              children: [
                                for (int i = 0; i < 3 ; i++) i == slideIndex ? _buildPageIndicator(true): _buildPageIndicator(false),
                              ],),
                          ),
                          SizedBox(height: 20),
                          InkWell(
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (BuildContext context) => LoginPage(),
                                ),
                              );
                            },
                            child: Container(
                              width: MediaQuery.of(context).size.width,
                              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 15),
                              decoration: BoxDecoration(
                                color: Color(CustomColor.offWhite),
                                borderRadius: BorderRadius.circular(5),
                              ),
                              child: Center(
                                child: Text(CustomStrings.button_login, textAlign: TextAlign.center,style: GoogleFonts.quicksand(
                                  textStyle: TextStyle(
                                    fontWeight: FontWeight.w500,
                                    fontSize: CustomSize.buttonText,
                                    color: Color(CustomColor.primaryDark),
                                  )
                                )),
                              ),
                            ),
                          ),
                          SizedBox(height: 20),
                          Divider(color: Color(CustomColor.white), thickness: .5),
                          Row(
                            children: [
                              TextButton(onPressed: (){
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (BuildContext context) => RegisterPage(),
                                  ),
                                );
                              }, child: Text(CustomStrings.button_register,style: GoogleFonts.quicksand(
                                textStyle: TextStyle(
                                  fontWeight: FontWeight.w500,
                                  fontSize: CustomSize.buttonText,
                                  color: Color(CustomColor.white),
                                )
                              ))),
                              Spacer(),
                              TextButton(onPressed: (){
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (BuildContext context) => SupportPageMain(),
                                  ),
                                );
                              }, child: Text("Get Help",style: GoogleFonts.quicksand(
                                textStyle: TextStyle(
                                  fontWeight: FontWeight.w500,
                                  fontSize: CustomSize.buttonText,
                                  color: Color(CustomColor.white),
                                )
                              ))),
                            ],
                          )
                        ],
                      ),
                    )
                ),
                Positioned(
                  top: 60,
                  right: 20,
                  child: GestureDetector(
                    child: Container(
                      color: Color(CustomColor.white),
                      padding: EdgeInsets.all(10),
                      child: Image.asset("assets/images/icon1.png", width: 40, fit: BoxFit.cover,),
                    )
                  )
                )
              ],
            )
        ),
      ),
    );
  }
}

class SlideTile extends StatelessWidget {
  final String imagePath, title, desc;

  SlideTile({this.imagePath, this.title, this.desc});

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;
    return Container(
      width: width,
      height: height,
      decoration: BoxDecoration(
        image: DecorationImage(
            image: AssetImage(imagePath),
            fit: BoxFit.cover
        ),
      ),
      child: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.bottomCenter,
                  end: Alignment.topCenter,
                  colors: [Color(CustomColor.black), Colors.transparent]
              )
          ),
        child: Stack(
          children: [
            Positioned(
              bottom: 200,
              left: 0,
              right: 0,
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(title, style: GoogleFonts.quicksand(
                      textStyle: TextStyle(
                        fontWeight: FontWeight.w700,
                        fontSize: CustomSize.giantText,
                        color: Color(CustomColor.white),
                      )
                    )),
                    SizedBox(height: 4),
                    Text(desc,style: GoogleFonts.ubuntu(
                      textStyle: TextStyle(
                        fontSize: CustomSize.buttonText,
                        color: Color(CustomColor.white),
                      )
                    )),
                  ],
                ),
              ),
            ),
          ],
        )
      )
    );
  }
}