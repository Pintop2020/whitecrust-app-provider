import 'data/custom_strings.dart';

class SliderModel{

  String imageAssetPath;
  String title, desc;

  SliderModel({this.imageAssetPath,this.title, this.desc});

  void setImageAssetPath(String getImageAssetPath){
    imageAssetPath = getImageAssetPath;
  }

  void setTitle(String getTitle){
    title = getTitle;
  }

  void setDesc(String getDesc){
    desc = getDesc;
  }

  String getImageAssetPath(){
    return imageAssetPath;
  }

  String getTitle(){
    return title;
  }

  String getDesc(){
    return desc;
  }

}


List<SliderModel> getSlides(){
  List<SliderModel> slides = [];
  SliderModel sliderModel = new SliderModel();
  sliderModel.setTitle(CustomStrings.intro_title_1);
  sliderModel.setImageAssetPath(CustomStrings.intro_image_1);
  sliderModel.setDesc(CustomStrings.intro_desc_1);
  slides.add(sliderModel);
  sliderModel = new SliderModel();
  sliderModel.setTitle(CustomStrings.intro_title_2);
  sliderModel.setImageAssetPath(CustomStrings.intro_image_2);
  sliderModel.setDesc(CustomStrings.intro_desc_2);
  slides.add(sliderModel);
  sliderModel = new SliderModel();
  sliderModel.setTitle(CustomStrings.intro_title_3);
  sliderModel.setImageAssetPath(CustomStrings.intro_image_3);
  sliderModel.setDesc(CustomStrings.intro_desc_3);
  slides.add(sliderModel);
  sliderModel = new SliderModel();
  return slides;
}