class CustomColor{
  static const int accent = 0xFFeb7d7c;
  static const int accentDark = 0xFFb71d1b;
  static const int primary = 0xFF1ba8df;
  static const int primaryDark = 0xFF081328;
  static const int white = 0xFFFFFFFF;
  static const int offBlack = 0xFF4E4E4E;
  static const int black = 0xFF000000;
  static const int black2 = 0xFF222222;
  static const int offBlack2 = 0xFFC4C4C4;
  static const int colorText = 0xFF27AE60;
  static const int danger = 0xFFBE0200;
  static const int googleButton = 0xFF4081EC;
  static const int facebookButton = 0xFF4267B2;
  //
  static const int success = 0xFF28A745;
  static const int warning = 0xFFFFC108;
  static const int secondary = 0xFF6D757D;
  static const int info = 0xFF18A2B8;
  static const int offWhite = 0xFFE0E0E0;
  //
  static const int greenThumb = 0xFFB5EAAA;
  static const int lightSalmon = 0xFFF9966B;
  static const int blueHosta = 0xFF77BFC7;
  static const int seaTurtle = 0xFF438D80;
  static const int chestnutRed = 0xFFC34A2C;
  static const int blueGreen = 0xFF7BCCB5;
  static const int mediumSeaGreen = 0xFF306754;
  static const int cantaloupe = 0xFFFFA62F;
  //
  static const int overDark = 0xFF080c11;
  static const int overDarkLight = 0xFF16202c;
  static const int overDarkMedium = 0xFF0e141c;
  static const int navigationBg = 0xFF252F49;
  //
  static const int bgTwo = 0xFFEFF0F4;
  static const int labelColor = 0xFF5E6C80;

}