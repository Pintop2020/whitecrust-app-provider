class CustomStrings {
  static const String tawk_direct_chat_link = "https://tawk.to/chat/5fa89a758e1c140c2abc084e/default";
  static const String currency = "₦"; //""\u{20A6}";
  // Intro screen text
  static const String intro_title_1 = "WHITECRUST LIMITED";
  static const String intro_desc_1 = "Stop being chained down by bad credit we have the key to set you free...";
  static const String intro_image_1 = "assets/images/personal_loans.jpeg";
  static const String intro_title_2 = "Easy & Quick Funds";
  static const String intro_desc_2 = "We offer financial services support to individuals, micro, small and medium enterprises.";
  static const String intro_image_2 = "assets/images/business_loans.jpeg";
  static const String intro_title_3 = "Look no further";
  static const String intro_desc_3 = "Facing Trouble in your daily life due to shortage of money? Worry no more, we are here to help solve all your financial problems";
  static const String intro_image_3 = "assets/images/advisory_services.jpeg";
// Button text
  static const String button_next = "Next";
  static const String button_login = 'Log In';
  static const String button_close = 'Close';
  static const String button_go = 'go';
  static const String request_new_password = 'Request new password';
  static const String button_proceed = "Proceed";
  static const String button_start = "Get Started";
  static const String button_register = "Sign up now";
  static const String button_register2 = "Register";
  // Action text
  static const String skip = "Skip";
  static const String back = "Back";
  static const String cancel = "Cancel";
  static const String save = "Save";
  // in-page title text
  static const String inPageLogin = "Log in";
  static const String inPageCreateAccount = "Create Account";
  static const String inPageForgotPassword = "Forgot Password";
  // form label
  static const String emailLabel = "Email";
  static const String passwordLabel = "Password";
  static const String showLabel = "show";
  static const String hideLabel = "hide";
  static const String emailSignIn = "Sign in with email";
  static const String forgetPass = "Forgot your password?";
  static const String facebookSignIn = "Sign in with Facebook";
  static const String appleSignIn = "Sign in with Apple";
  static const String createAccount = "Create a new account";
  static const String firstNameLabel = "First Name";
  static const String lastNameLabel = "Last Name";
  static const String phoneLabel = "Phone Number";
  static const String signIn = "Sign In";
  // error title
  static const String oops = "Oops!";
  static const String hurray = "Hurray!";
  // error texts
  static const String errorEmail = "Please enter a valid email address";
  static const String passwordLengthError = "Password must be minimum of 8 characters";
  static const String nameError = "Please enter your full name";
  static const String phoneError = "Please enter valid phone number";
  // success texts
  static const String accountSuccess = "Your account was created successfully";
  // Loader text
  static const String loading = "LOADING";
  static const String saving = "SAVING";
}