import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:white_crust/data/custom_icons_icons.dart';
import 'custom_color.dart';
import 'custom_sizes.dart';
import 'dart:core';
import '../data/custom_color.dart';
import '../data/custom_sizes.dart';
import 'package:flutter/cupertino.dart';
import 'dart:async';
import 'dart:io';
import 'login_session.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:white_crust/data/login_session.dart';
import 'package:flutter/rendering.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:intl/intl.dart';
import 'package:flutter_statusbar_manager/flutter_statusbar_manager.dart';


// All Url Links and server keys
////////////////////////////////////////////////////////////////////////////////
class UrlLinks {
  static const String mainSite = "https://my.whitecrust.com.ng/";
  static const String mediaSite = "https://my.whitecrust.com.ng/storage/";
  static const String site = "https://my.whitecrust.com.ng/api/v1/";
  static const String register = "auth/register";
  static const String verify = "auth/verify";
  static const String login = "auth/login";
  static const String check_token = "auth/check_token";
  static const String create_token = "auth/create_token";
  static const String forget = "auth/forget";
  static const String reset = "auth/reset";

  static const String getUser = "profile";
  static const String banks = "banks/all";
  static const String logout = "logout";
  static const String loans = "loans";
  static const String singleLoan = "loans/single";
  static const String applyLoans = "loans/apply";
  static const String checkInterval = "loans/check_interval";
  static const String loanHistory = "loans/trans";
  static const String readNotif = "read_notifications";
}
////////////////////////////////////////////////////////////////////////////////
// All Url Link ends

// Custom loader widget
////////////////////////////////////////////////////////////////////////////////
class LoaderPage extends StatefulWidget {
  @override
  _LoaderPage createState() => _LoaderPage();
}
class _LoaderPage extends State<LoaderPage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        backgroundColor: Colors.black45,
        body: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: Center(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SpinKitWave(color: Color(CustomColor.info)),
                  ],
                )
            )
        ),
      ),
    );
  }
}

class AlwaysDisabledFocusNode extends FocusNode {
  @override
  bool get hasFocus => false;
}


void show(BuildContext context) {
  showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) => LoaderPage());
}

void hide(BuildContext context) {
  Navigator.of(context).pop();
}

Future<Map<String, String>> getHeader() async {
  LoginSessionManager loginSessionManager = new LoginSessionManager();
  final Map<String, String> headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
    "Authorization": "Bearer ${await loginSessionManager.getToken()}",
  };
  return headers;
}

Future<Map<String, String>> getHeaderImage() async {
  final LoginSessionManager loginSessionManager = new LoginSessionManager();
  final Map<String, String> headers = {
    "Accept": "application/json",
    "Content-type": "multipart/form-data",
    "Authorization": "Bearer ${await loginSessionManager.getToken()}",
  };
  return headers;
}

Future<Map<String, String>> getSimpleHeader() async {
  final Map<String, String> headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
  };
  return headers;
}

class Notifications{
  int notifiable_id;
  String id, type, notifiable_type, read_at, created_at, updated_at;
  NotificationsData data;
  Notifications({this.id, this.type, this.notifiable_type, this.read_at, this.notifiable_id, this.created_at, this.updated_at, this.data});
  factory Notifications.fromJson(Map<String, dynamic> json){
    return Notifications(
      id: json["id"],
      type: json["type"],
      notifiable_type: json["notifiable_type"],
      notifiable_id: int.parse("${json["notifiable_id"]}"),
      data: NotificationsData.fromJson(json["data"]),
      read_at: json["read_at"],
      created_at: json["created_at"],
      updated_at: json["updated_at"],
    );
  }
}

class NotificationsData{
  String body, icon, title;
  NotificationsData({this.body, this.icon, this.title});
  factory NotificationsData.fromJson(Map<String, dynamic> json){
    return NotificationsData(
      body: json["body"],
      icon: json["icon"],
      title: json["title"]
    );
  }
}

String getTimeConverted(String dateString, {bool numericDates = true}) {
  DateTime date = DateTime.parse(dateString);
  final date2 = DateTime.now();
  final difference = date2.difference(date);

  if (difference.inDays > 8) {
    return '${date.year}/${date.month}/${date.day}';
  } else if ((difference.inDays / 7).floor() >= 1) {
    return (numericDates) ? '1 week ago' : 'Last week';
  } else if (difference.inDays >= 2) {
    return '${difference.inDays} days ago';
  } else if (difference.inDays >= 1) {
    return (numericDates) ? '1 day ago' : 'Yesterday';
  } else if (difference.inHours >= 2) {
    return '${difference.inHours} hours ago';
  } else if (difference.inHours >= 1) {
    return (numericDates) ? '1 hour ago' : 'An hour ago';
  } else if (difference.inMinutes >= 2) {
    return '${difference.inMinutes} minutes ago';
  } else if (difference.inMinutes >= 1) {
    return (numericDates) ? '1 minute ago' : 'A minute ago';
  } else if (difference.inSeconds >= 3) {
    return '${difference.inSeconds} seconds ago';
  } else {
    return 'Just now';
  }
}

String getTimeConverted2(String dateString) {
  DateTime date = DateTime.parse(dateString);

  return "${date.day} ${monthNamesShort[date.month-1]}, ${date.year}";
}

class BanksFull{
  int id;
  Banks data;
  String createdAt, updatedAt;
  bool isDefault;
  BanksFull({this.id, this.data,
    this.createdAt, this.updatedAt, this.isDefault});
  factory BanksFull.fromJson(Map<String, dynamic> json){
    return BanksFull(
      id: json["id"],
      data: Banks.fromJson(jsonDecode(json["details"])['bank']),
      createdAt: json["created_at"],
      updatedAt: json["updated_at"],
      isDefault: json["is_default"] == 0 ? false : true,
    );
  }
}

class Banks{
  String name, code;
  int id;
  Banks({this.id, this.code, this.name});
  factory Banks.fromJson(Map<String, dynamic> json){
    return Banks(
        id: json["id"],
        code: json["code"],
        name: json["name"]
    );
  }
  Map<String, dynamic> toJson() => {
    "id": id,
    "name": name,
    "code": code
  };
}

class Cards{
  int id;
  CardsData data;
  String created_at, updated_at;
  bool isDefault;
  Cards({this.id, this.data, this.created_at, this.updated_at, this.isDefault});
  factory Cards.fromJson(Map<String, dynamic> json){
    return Cards(
      id: json["id"],
      data: CardsData.fromJson(jsonDecode(json["details"])),
      created_at: json["created_at"],
      updated_at: json["updated_at"],
      isDefault: json["is_default"] == 0 ? false : true,
    );
  }
}

class CardsData{
  String cardNumber, cvv, pin, expiryMonth, expiryYear, brand, embedToken, type;
  CardsData({this.cardNumber, this.cvv, this.pin, this.expiryMonth, this.expiryYear, this.brand, this.embedToken, this.type});
  factory CardsData.fromJson(Map<String, dynamic> json){
    return CardsData(
        cardNumber: json["card_number"],
        cvv: json["cvv"],
        pin: json["pin"],
        expiryMonth: json["expiry_month"],
        expiryYear: json["expiry_year"],
        brand: json["brand"],
        embedToken: json["embed_token"],
        type: json['type']
    );
  }
}

showAlert(BuildContext context, {String title, String message, bool isAction: false, String action, GestureTapCallback onTap, bool isCuperTino: false}) async {
  if(isAction){
    final result = await showOkCancelAlertDialog(
      context: context,
      title: title,
      message: message,
      //defaultType: OkCancelAlertDefaultType.cancel,
      useActionSheetForCupertino: isCuperTino,
      okLabel: 'Cancel',
      cancelLabel: action,
      barrierDismissible: false,
      isDestructiveAction: true,
    );
    if(result == OkCancelResult.cancel){
      onTap();
    }
  }else {
    await showOkAlertDialog(
        context: context,
        title: title,
        message: message,
        barrierDismissible: false,
        okLabel: 'Close!',
        useActionSheetForCupertino: isCuperTino
    );
  }
}


List monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
List monthNamesShort = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

void showInSnackBar(BuildContext context, String value, Color textColor, Color bgColor) {
  ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(value, style: TextStyle(
      fontWeight: FontWeight.w500,
      fontSize: CustomSize.buttonText,
      color: textColor,
      fontFamily: 'Poppins'
  )), backgroundColor: bgColor));
}

void launchURL(String _url) async =>
    await canLaunch(_url) ? await launch(_url) : throw 'Could not launch $_url';

statusContent(BuildContext context, {bool isError: false, String text, GestureTapCallback onTap, String actionText, bool isWarning: false}){
  return Container(
    margin: EdgeInsets.only(top: 20),
    padding: EdgeInsets.symmetric(vertical: 20, horizontal: 30),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Center(
          child: Container(
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: Color(isWarning ? CustomColor.warning : isError ? CustomColor.danger : CustomColor.success)
            ),
            padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
            child: Icon(isError || isWarning ? CustomIcons.info : Icons.check, color: Color(CustomColor.white), size: 20),
          ),
        ),
        SizedBox(height: 20),
        Text("$text\n\n\n\n", textAlign: TextAlign.center, maxLines: 4, overflow: TextOverflow.ellipsis, style: GoogleFonts.quicksand(textStyle: TextStyle(
          fontWeight: FontWeight.w300,
          fontSize: CustomSize.bigTitle,
          color: Color(CustomColor.offWhite),
        ))),
        SizedBox(height: 20),
        GestureDetector(
          onTap: onTap,
          child: Container(
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: Color(CustomColor.black2).withOpacity(.2),
            ),
            padding: EdgeInsets.symmetric(vertical: 15, horizontal: 30),
            child: Text(actionText, textAlign: TextAlign.center, style: GoogleFonts.quicksand(textStyle: TextStyle(
              fontWeight: FontWeight.w600,
              fontSize: CustomSize.buttonText,
              color: Color(CustomColor.black2),
            ))),
          ),
        ),
        if(actionText.toLowerCase() != "close") SizedBox(height: 10),
        if(actionText.toLowerCase() != "close") GestureDetector(
            onTap: (){
              Navigator.of(context).pop();
            },
            child: Text("Close", textAlign: TextAlign.center, style: GoogleFonts.quicksand(textStyle: TextStyle(
              fontWeight: FontWeight.w500,
              fontSize: CustomSize.normalText,
              color: Color(CustomColor.danger),
            )))
        ),
      ],
    ),
  );
}

void showSuccess(BuildContext context, {bool isError: false, String text, GestureTapCallback onTap, String actionText, bool isWarning: false}) {
  showModalBottomSheet(
      elevation: 10,
      backgroundColor: Color(CustomColor.info),
      context: context,
      isDismissible: false,
      builder: (context) => Container(
          width: MediaQuery.of(context).size.width,
          height: 400,
          color: Color(CustomColor.info),
          alignment: Alignment.center,
          child: statusContent(context, isError: isError, text: text, onTap: onTap, actionText: actionText, isWarning: isWarning)
      )
  );
}

class ApiBaseHelper {

  Future<dynamic> get(BuildContext context, String url, var headers, {bool isLoader: false}) async {
    var responseJson;
    try{
      if(isLoader) show(context);
      final response = await http.get(Uri.parse(UrlLinks.site + url), headers: headers);
      print(response.body);
      responseJson = _returnResponse(context, response, isLoader: isLoader);
    }on SocketException{
      if(isLoader) hide(context);
      responseJson = {"status":false, "message": "No internet connection"};
    }on HandshakeException {
      if(isLoader) hide(context);
      responseJson = {"status":false, "message": "No internet connection"};
    }
    return responseJson;
  }

  Future<dynamic> post(BuildContext context, String url, var body, var headers, {bool isLoader: false}) async {
    var responseJson;
    try{
      if(isLoader) show(context);
      final response = await http.post(Uri.parse(UrlLinks.site + url), body: body, headers: headers);
      print(response.body);
      responseJson = _returnResponse(context, response, isLoader: isLoader);
    }on SocketException{
      if(isLoader) hide(context);
      responseJson = {"status":false, "message": "No internet connection"};
    }on HandshakeException {
      if(isLoader) hide(context);
      responseJson = {"status":false, "message": "No internet connection"};
    }
    return responseJson;
  }

  Future<dynamic> patch(BuildContext context, String url, var body, var headers, {bool isLoader: false}) async {
    var responseJson;
    try{
      if(isLoader) show(context);
      final response = await http.patch(Uri.parse(UrlLinks.site + url), body: body, headers: headers);
      print(response.body);
      responseJson = _returnResponse(context, response, isLoader: isLoader);
    }on SocketException{
      if(isLoader) hide(context);
      responseJson = {"status":false, "message": "No internet connection"};
    }on HandshakeException {
      if(isLoader) hide(context);
      responseJson = {"status":false, "message": "No internet connection"};
    }
    return responseJson;
  }

  dynamic _returnResponse(BuildContext context, http.Response response, {bool isLoader: false}) {
    if(isLoader) hide(context);
    switch (response.statusCode) {
      case 201:
      case 200:
        var responseJson = json.decode(response.body.toString());
        return responseJson;
      case 400:
        var responseJson = {"status":false, "message": response.body.toString()};
        return responseJson;
      case 401:
      case 403:
        var responseJson = {"status":false, "message": "Unauthenticated"};
        return responseJson;
      case 500:
      default:
        var responseJson = {"status":false, "message": "Error occurred while Communication with Server"};
        return responseJson;
    }
  }
}

List<int> randomColor = [CustomColor.greenThumb, CustomColor.lightSalmon, CustomColor.blueHosta, CustomColor.seaTurtle, CustomColor.chestnutRed,
CustomColor.blueGreen, CustomColor.mediumSeaGreen, CustomColor.cantaloupe];

String toK(var number){
  var numberToFormat = int.parse("$number");
  var _formattedNumber = NumberFormat.compactCurrency(
    decimalDigits: 0,
    symbol: '', // if you want to add currency symbol then pass that in this else leave it empty.
  ).format(numberToFormat);
  return _formattedNumber;
}

Future<String> getCaches(String key) async{
  var val = '';
  LoginSessionManager loginSessionManager = new LoginSessionManager();
  var data = await loginSessionManager.getSystemCaches();
  if(data != ''){
    var decoded = jsonDecode(data);
    if(decoded[key] != null && decoded[key] != '') val = jsonEncode(decoded[key]);
    else val = '';
  }
  return val;
}

setCaches(var value, String key) async{
  LoginSessionManager loginSessionManager = new LoginSessionManager();
  var data = await loginSessionManager.getSystemCaches();
  var val = '';
  if(data != ''){
    var decoded = jsonDecode(data);
    decoded[key] = value;
    val = jsonEncode(decoded);
  }else {
    var decoded = {};
    decoded[key] = value;
    val = jsonEncode(value);
  }
  await loginSessionManager.setSystemCaches(val);
}

setStatusBarTextColor() async {
  StatusBarStyle _statusBarStyle = StatusBarStyle.LIGHT_CONTENT;
  FlutterStatusbarManager.setStyle(_statusBarStyle);
}

Widget status(var data){
  return Container(
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(10),
      color: Color(getColorForLoan(data['status']['status'])),
    ),
    padding: EdgeInsets.symmetric(vertical: 5, horizontal: 20),
    child: Text(data['status']['status'].toUpperCase(), style: TextStyle(
        fontWeight: FontWeight.w500,
        fontSize: CustomSize.normalText,
        color: Color(CustomColor.white),
        fontFamily: 'Poppins'
    )),
  );
}

String convertToLast(String value){
  var data = '';
  if(value == 'Last 7 days'){
    data = '7';
  }else if(value == 'Last 30 days'){
    data = '30';
  }else if(value == 'Yesterday'){
    data = '2';
  }else if(value == 'Today'){
    data = '1';
  }else if(value == 'Last 14 days'){
    data = '14';
  }else {
    data = 'all';
  }
  return data;
}

String getTransName(var decoded){
  var ret = '';
  var type = decoded['type'];
  if(type == 'npf'){
    ret = 'NPF';
  }else if(type == 'lasg'){
    ret = "LAG";
  }else if(type == 'custom'){
    ret = 'CTM';
  }else if(type == 'nscdc'){
    ret = 'NSC';
  }else {
    ret = 'OTH';
  }
  return ret;
}

String getTransDesc(var decoded){
  var ret = '';
  var type = decoded['type'];
  if(type == 'npf'){
    ret = 'Nigeria Police Force Loan';
  }else if(type == 'lasg'){
    ret = "Lagos State Government / Federal Workers Loan";
  }else if(type == 'custom'){
    ret = 'Custom Officers Loan';
  }else if(type == 'nscdc'){
    ret = 'Nigeria Security and Civil Defence Corps';
  }else {
    ret = 'Others';
  }
  return ret;
}

int getColorForLoan(var status){
  var ret = CustomColor.warning;
  if(status == 'pending'){
    ret = CustomColor.warning;
  }else if(status == 'declined'){
    ret = CustomColor.danger;
  }else if(status == 'active'){
    ret = CustomColor.primary;
  }else if(status == 'repaid'){
    ret = CustomColor.success;
  }else if(status == 'approved'){
    ret = CustomColor.info;
  }
  return ret;
}

class PushNotification {
  PushNotification({
    this.title,
    this.body,
  });
  String title;
  String body;
}

enum Status { LOADING, COMPLETED, ERROR }

/// Create a [AndroidNotificationChannel] for heads up notifications
const AndroidNotificationChannel channel = AndroidNotificationChannel(
  'high_importance_channel', // id
  'High Importance Notifications', // title
  'This channel is used for important notifications.', // description
  importance: Importance.high,
);

/// Initialize the [FlutterLocalNotificationsPlugin] package.
final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
FlutterLocalNotificationsPlugin();