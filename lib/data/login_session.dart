import 'package:shared_preferences/shared_preferences.dart';

class LoginSessionManager {

  String isLoggedIn = "is_logged";
  String isBiometrics = "is_biometrics";
  String token = "token";
  String appPin = "app_pin";
  String otherData = "other_data";
  String systemCaches = "system_caches";

  Future<void> logUserIn() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool(isLoggedIn, true);
  }
  Future<bool> isUserLogged() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool boolValue = prefs.getBool(isLoggedIn) ?? false;
    return boolValue;
  }

  Future<void> setBioMetrics() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool(isBiometrics, true);
  }
  Future<bool> getBioMetrics() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool boolValue = prefs.getBool(isBiometrics) ?? false;
    return boolValue;
  }

  Future<void> setToken(String value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(token, value ?? '');
  }
  Future<String> getToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String stringValue = prefs.getString(token) ?? '';
    return stringValue;
  }

  Future<void> setAppPin(String value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(appPin, value ?? '');
  }
  Future<String> getAppPin() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String stringValue = prefs.getString(appPin) ?? '';
    return stringValue;
  }

  Future<void> setOtherData(String value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(otherData, value ?? '');
  }
  Future<String> getOtherData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String stringValue = prefs.getString(otherData) ?? '';
    return stringValue;
  }

  Future<void> logout() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.clear();
  }

  Future<void> setSystemCaches(String value) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString(systemCaches, value ?? '');
  }
  Future<String> getSystemCaches() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String stringValue = prefs.getString(systemCaches) ?? '';
    return stringValue;
  }
}