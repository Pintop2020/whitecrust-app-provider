class CustomSize {
  static const double buttonText = 16;
  static const double mediumText = 22;
  static const double giantText = 25;
  static const double giantTextDe = 24;
  static const double mediumTextMin = 20;
  static const double bigTitle = 30;
  static const double smallText = 12;
  static const double bigText = 18;
  static const double normalText = 14;
  static const double normalText2 = 15;
  static const double smallestTest = 10;
  static const double biggestText = 28;
  static const double maxMinSmall = 35;
  static const double maxMin = 40;
  static const double max = 54;
}