import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:white_crust/auth/verify.dart';
import '../data/custom_color.dart';
import '../data/custom_sizes.dart';
import '../data/custom_strings.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/cupertino.dart';
import '../data/utils.dart';
import 'package:international_phone_input/international_phone_input.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';
import 'package:white_crust/data/custom_icons_icons.dart';
import 'package:http/http.dart' as http;

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPage createState() => _RegisterPage();
}

class _RegisterPage extends State<RegisterPage>
    with WidgetsBindingObserver, TickerProviderStateMixin{

  double _progressWidth = 40;
  int slideIndex = 0;
  PageController controller;
  String phoneNumber;
  String phoneIsoCode = '+234';
  File selectedPassport;
  final picker = ImagePicker();
  bool _noShowPassword = true;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  void onPhoneNumberChange(
      String number, String internationalizedPhoneNumber, String isoCode) {
    if(mounted)setState(() {
      phoneNumber = number;
      phoneIsoCode = isoCode;
    });
  }

  TextEditingController fnameController = new TextEditingController(),
      lnameController = new TextEditingController(),
      mnameController = new TextEditingController(),
      phoneController = new TextEditingController(),
      emailController = new TextEditingController(),
      passwordController = new TextEditingController();
  String fnameErr, lnameErr, phoneErr, emailErr, passwordErr, passportErr;

  void _imgFromCamera() async {
    final pickedFile = await picker.getImage(source: ImageSource.camera, maxHeight: 200,
        maxWidth: 200,
        imageQuality: 50);
    if (mounted) setState(() {
      if (pickedFile != null) {
        selectedPassport = File(pickedFile.path);
      }
    });
  }

  Widget _showErr(String error){
    return Text(error, style: GoogleFonts.quicksand(
      textStyle: TextStyle(
        fontWeight: FontWeight.w500,
        fontSize: CustomSize.smallText,
        color: Color(CustomColor.danger),
      )
    ));
  }

  Widget _input(TextEditingController controller, String label,
      {bool isEMail: false, bool isPassword: false, IconData iconData}) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          TextField(
            style: GoogleFonts.quicksand(
              textStyle: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: CustomSize.normalText,
                color: Color(CustomColor.black2),
              )
            ),
            obscureText: isPassword ? _noShowPassword : false,
            controller: controller,
            keyboardType: isEMail ? TextInputType.emailAddress : TextInputType.text,
            decoration: InputDecoration(
              hintText: label,
              hintStyle: GoogleFonts.quicksand(
                textStyle: TextStyle(
                  fontSize: CustomSize.normalText,
                )
              ),
              prefixIcon: Icon(iconData),
              filled: false,
              contentPadding: EdgeInsets.only(top: 10, bottom: 10, right: 10),
              suffix: isPassword ? InkWell(
                onTap: () {
                  if(mounted) setState(() {
                    _noShowPassword = !_noShowPassword;
                  });
                },
                child: Icon(
                  _noShowPassword
                      ? CupertinoIcons.eye_slash
                      : CupertinoIcons.eye, color: Color(CustomColor.primary),
                ),
              ) : null,
            ),
          )
        ],
      ),
    );
  }

  Widget _entryField(){
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(height: 40),
          Text("What's your first name?", textAlign: TextAlign.center,style: GoogleFonts.quicksand(
            textStyle: TextStyle(
              fontWeight: FontWeight.w800,
              fontSize: CustomSize.bigTitle,
              color: Color(CustomColor.offBlack),
            )
          )),
          SizedBox(height: 70),
          _input(fnameController, "First Name", iconData: CustomIcons.user_1),
          if(fnameErr != null) _showErr(fnameErr),
          SizedBox(height: 40),
          GestureDetector(
            onTap: (){
              if(fnameController.text.length < 3){
                if(mounted)
                setState(() {
                  fnameErr = "Please enter your first name";
                });
              }else {
                if(mounted)
                  setState(() {
                    fnameErr = null;
                  _progressWidth = _progressWidth+40;
                  slideIndex = slideIndex+1;
                });
                controller.animateToPage(slideIndex, duration: Duration(milliseconds: 500), curve: Curves.linear);
              }
            },
            child: Container(
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Color(CustomColor.primaryDark)
                ),
                padding: EdgeInsets.symmetric(vertical: 10, horizontal: 30),
                child: Center(
                  child: Text("NEXT", style: GoogleFonts.quicksand(
                    textStyle: TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: CustomSize.buttonText,
                      color: Color(CustomColor.offWhite),
                    )
                  )),
                )
            ),
          )
        ],
      ),
    );
  }

  Widget _entryField2(){
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(height: 40),
          Text("What's your last name?", textAlign: TextAlign.center,style: GoogleFonts.quicksand(
              textStyle: TextStyle(
                fontWeight: FontWeight.w800,
                fontSize: CustomSize.bigTitle,
                color: Color(CustomColor.offBlack),
              )
          )),
          SizedBox(height: 70),
          _input(lnameController, "Last Name", iconData: CustomIcons.user_1),
          if(lnameErr != null) _showErr(lnameErr),
          SizedBox(height: 40),
          GestureDetector(
            onTap: (){
              if(lnameController.text.length < 3){
                if(mounted)
                  setState(() {
                    lnameErr = "Please enter your last name";
                  });
              }else {
                if(mounted)
                  setState(() {
                    lnameErr = null;
                    _progressWidth = _progressWidth+40;
                    slideIndex = slideIndex+1;
                  });
                controller.animateToPage(slideIndex, duration: Duration(milliseconds: 500), curve: Curves.linear);
              }
            },
            child: Container(
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Color(CustomColor.primaryDark)
                ),
                padding: EdgeInsets.symmetric(vertical: 10, horizontal: 30),
                child: Center(
                  child: Text("NEXT", style: GoogleFonts.quicksand(
                      textStyle: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: CustomSize.buttonText,
                        color: Color(CustomColor.offWhite),
                      )
                  )),
                )
            ),
          )
        ],
      ),
    );
  }

  Widget _entryField3(){
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(height: 40),
          Text("What's your middle name?", textAlign: TextAlign.center,style: GoogleFonts.quicksand(
              textStyle: TextStyle(
                fontWeight: FontWeight.w800,
                fontSize: CustomSize.bigTitle,
                color: Color(CustomColor.offBlack),
              )
          )),
          SizedBox(height: 70),
          _input(mnameController, "Other Names", iconData: CustomIcons.user_1),
          SizedBox(height: 40),
          GestureDetector(
            onTap: (){
              if(mounted)
                setState(() {
                  _progressWidth = _progressWidth+40;
                  slideIndex = slideIndex+1;
                });
              controller.animateToPage(slideIndex, duration: Duration(milliseconds: 500), curve: Curves.linear);
            },
            child: Container(
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Color(CustomColor.primaryDark)
                ),
                padding: EdgeInsets.symmetric(vertical: 10, horizontal: 30),
                child: Center(
                  child: Text("NEXT", style: GoogleFonts.quicksand(
                      textStyle: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: CustomSize.buttonText,
                        color: Color(CustomColor.offWhite),
                      )
                  )),
                )
            ),
          )
        ],
      ),
    );
  }

  Widget _entryField4(){
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(height: 40),
          Text("What's your email address?", textAlign: TextAlign.center,style: GoogleFonts.quicksand(
              textStyle: TextStyle(
                fontWeight: FontWeight.w800,
                fontSize: CustomSize.bigTitle,
                color: Color(CustomColor.offBlack),
              )
          )),
          SizedBox(height: 70),
          _input(emailController, "Email", iconData: CustomIcons.mail),
          if(emailErr != null) _showErr(emailErr),
          SizedBox(height: 40),
          GestureDetector(
            onTap: (){
              if(!EmailValidator.validate(emailController.text)){
                if(mounted)
                  setState(() {
                    emailErr = "Please enter your valid email address";
                  });
              }else {
                if(mounted)
                  setState(() {
                    emailErr = null;
                  _progressWidth = _progressWidth+40;
                  slideIndex = slideIndex+1;
                });
                controller.animateToPage(slideIndex, duration: Duration(milliseconds: 500), curve: Curves.linear);
              }
            },
            child: Container(
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Color(CustomColor.primaryDark)
                ),
                padding: EdgeInsets.symmetric(vertical: 10, horizontal: 30),
                child: Center(
                  child: Text("NEXT", style: GoogleFonts.quicksand(
                      textStyle: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: CustomSize.buttonText,
                        color: Color(CustomColor.offWhite),
                      )
                  )),
                )
            ),
          )
        ],
      ),
    );
  }

  Widget _entryField5(){
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(height: 40),
          Text("What's your mobile number?", textAlign: TextAlign.center,style: GoogleFonts.quicksand(
              textStyle: TextStyle(
                fontWeight: FontWeight.w800,
                fontSize: CustomSize.bigTitle,
                color: Color(CustomColor.offBlack),
              )
          )),
          SizedBox(height: 70),
          Container(
            decoration: BoxDecoration(
                border: Border(
                    bottom: BorderSide(
                        color: Color(CustomColor.black2).withOpacity(.4)
                    )
                )
            ),
            padding: EdgeInsets.symmetric(horizontal: 10),
            child: InternationalPhoneInput(
              decoration: InputDecoration.collapsed(
                hintText: 'Mobile number',
                hintStyle: GoogleFonts.quicksand(
                  textStyle: TextStyle(
                    fontSize: CustomSize.normalText,
                  )
                ),
                filled: false,
              ),
              onPhoneNumberChange: onPhoneNumberChange,
              initialPhoneNumber: phoneNumber,
              initialSelection: phoneIsoCode,
              enabledCountries: ['+234'],
              showCountryCodes: true,
              showCountryFlags: true,
            ),
          ),
          if(phoneErr != null) _showErr(phoneErr),
          SizedBox(height: 40),
          GestureDetector(
            onTap: (){
              if(phoneNumber.length < 10 || phoneNumber.length > 10){
                setState(() {
                  phoneErr = "Please enter valid phone number";
                });
              }else {
                if(mounted)
                  setState(() {
                    phoneErr = null;
                  _progressWidth = _progressWidth+40;
                  slideIndex = slideIndex+1;
                });
                controller.animateToPage(slideIndex, duration: Duration(milliseconds: 500), curve: Curves.linear);
              }
            },
            child: Container(
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Color(CustomColor.primaryDark)
                ),
                padding: EdgeInsets.symmetric(vertical: 10, horizontal: 30),
                child: Center(
                  child: Text("NEXT", style: GoogleFonts.quicksand(
                      textStyle: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: CustomSize.buttonText,
                        color: Color(CustomColor.offWhite),
                      )
                  )),
                )
            ),
          )
        ],
      ),
    );
  }

  Widget _entryField6(){
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(height: 40),
          Text("Enter your password of choice", textAlign: TextAlign.center,style: GoogleFonts.quicksand(
              textStyle: TextStyle(
                fontWeight: FontWeight.w800,
                fontSize: CustomSize.bigTitle,
                color: Color(CustomColor.offBlack),
              )
          )),
          SizedBox(height: 70),
          _input(passwordController, "Password", iconData: CustomIcons.lock, isPassword: true),
          if(passwordErr != null) _showErr(passwordErr),
          SizedBox(height: 40),
          GestureDetector(
            onTap: (){
              if(passwordController.text.isEmpty || passwordController.text.length < 8){
                if(mounted)
                  setState(() {
                    passwordErr = CustomStrings.passwordLengthError;
                  });
              }else {
                if(mounted)
                  setState(() {
                    passwordErr = null;
                    _progressWidth = _progressWidth+40;
                    slideIndex = slideIndex+1;
                  });
                controller.animateToPage(slideIndex, duration: Duration(milliseconds: 500), curve: Curves.linear);
              }
            },
            child: Container(
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Color(CustomColor.primaryDark)
                ),
                padding: EdgeInsets.symmetric(vertical: 10, horizontal: 30),
                child: Center(
                  child: Text("NEXT", style: GoogleFonts.quicksand(
                      textStyle: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: CustomSize.buttonText,
                        color: Color(CustomColor.offWhite),
                      )
                  )),
                )
            ),
          )
        ],
      ),
    );
  }

  Widget _entryField7(){
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(height: 40),
          Text("Please select a passport photograph?", textAlign: TextAlign.center,style: GoogleFonts.quicksand(
              textStyle: TextStyle(
                fontWeight: FontWeight.w800,
                fontSize: CustomSize.bigTitle,
                color: Color(CustomColor.offBlack),
              )
          )),
          SizedBox(height: 70),
          GestureDetector(
              onTap: _imgFromCamera,
              child: selectedPassport != null ? ClipRRect(
                borderRadius: BorderRadius.circular(150),
                child: Image.file(
                  selectedPassport,
                  width: 150,
                  height: 150,
                  fit: BoxFit.cover,
                ),
              ) : Container(
                width: 150,
                height: 150,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(150),
                  image: DecorationImage(
                    image: AssetImage("assets/images/avatar.png"),
                    fit: BoxFit.cover,
                  ),
                ),
              )
          ),
          if(passportErr != null) _showErr(passportErr),
          SizedBox(height: 40),
          GestureDetector(
            onTap: (){
              if(selectedPassport == null){
                setState(() {
                  passportErr = "Please upload a passport photograph";
                });
              }else {
                passportErr = null;
                _submitData();
              }
            },
            child: Container(
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Color(CustomColor.primaryDark)
                ),
                padding: EdgeInsets.symmetric(vertical: 10, horizontal: 30),
                child: Center(
                  child: Text("SUBMIT", style: GoogleFonts.quicksand(
                      textStyle: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: CustomSize.buttonText,
                        color: Color(CustomColor.offWhite),
                      )
                  )),
                )
            ),
          )
        ],
      ),
    );
  }


  _submitData() async{
    Map<String, String> data = {
      "first_name": fnameController.text,
      "last_name": lnameController.text,
      "other_name": mnameController.text,
      "email": emailController.text,
      "mobile": phoneNumber,
      "password": passwordController.text,
    };
    show(context);
    var uri = Uri.parse(UrlLinks.site+UrlLinks.register);
    var request = http.MultipartRequest('POST', uri)
      ..headers.addAll(await getHeaderImage())
      ..fields.addAll(data)
      ..files.addAll({
        await http.MultipartFile.fromPath(
            'passport', selectedPassport.path)
      });
    var response = await request.send();
    hide(context);
    response.stream.transform(utf8.decoder).listen((value) {
      final dec = jsonDecode(value);
      if(dec['status'] != null && !dec['status']){
        if(dec['message']['email'][0] != null){
          showSuccess(context, text: dec['message']['email'][0], actionText: "Close", isError: true, onTap: (){
            Navigator.of(context).pop();
          });
        }else if(dec['message']['mobile'][0] != null){
          showSuccess(context, text: dec['message']['mobile'][0], actionText: "Close", isError: true, onTap: (){
            Navigator.of(context).pop();
          });
        }
      }else if(dec['status'] != null && dec['status']) {
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (BuildContext context) =>
                  VerificationPage(user: dec['data'])
          ),
        );
      }else {
        showSuccess(context, text: "Unknown server errors!", actionText: "Close", isError: true, onTap: (){
          Navigator.of(context).pop();
        });
      }
    });
  }

  Widget _progress(){
    return Container(
      width: _progressWidth,
      height: 10.0,
      decoration: BoxDecoration(
        color: Color(CustomColor.offWhite),
        borderRadius: BorderRadius.circular(90),
      ),
      child: Container(
        decoration: BoxDecoration(
          color: Color(CustomColor.primary),
          borderRadius: BorderRadius.circular(90),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;
    return AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.dark,
        child: Scaffold(
            key: _scaffoldKey,
            appBar: AppBar(
              backgroundColor: Color(CustomColor.white),
              elevation: 0.0,
              title: _progress(),
              iconTheme: IconThemeData(
                color: Color(CustomColor.primary), //change your color here
              ),
              leading: slideIndex == 0 ? IconButton(
                icon: Icon(Icons.clear, color: Color(CustomColor.offBlack2)),
                onPressed: () => Navigator.of(context).pop(),
              ) : IconButton(
                icon: Icon(Icons.arrow_back, color: Color(CustomColor.offBlack2)),
                onPressed: () {
                  if(mounted)setState(() {
                    slideIndex = slideIndex-1;
                  });
                  controller.animateToPage(slideIndex, duration: Duration(milliseconds: 500), curve: Curves.linear);
                },
              ),
            ),
            body: GestureDetector(
              onTap: () {
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              onVerticalDragDown: (DragDownDetails details){
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              onVerticalDragStart: (DragStartDetails details){
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              child:Container(
                  width: width,
                  height: height,
                  color: Color(CustomColor.white),
                  child: SingleChildScrollView(
                    child: Container(
                        width: width,
                        height: height-140,
                        //margin: EdgeInsets.only(bottom: 70),
                        padding: EdgeInsets.symmetric(horizontal: 20),
                        child: PageView(
                          physics:new NeverScrollableScrollPhysics(),
                          controller: controller,
                          onPageChanged: (index) {
                            if(mounted)setState(() {
                              slideIndex = index;
                            });
                          },
                          children: <Widget>[
                            _entryField(),
                            _entryField2(),
                            _entryField3(),
                            _entryField4(),
                            _entryField5(),
                            _entryField6(),
                            _entryField7()
                          ],
                        )
                    ),
                  )
              ),
            )
        )
    );
  }

  @override
  void initState() {
    super.initState();
    controller = new PageController();
  }
}