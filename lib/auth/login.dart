import 'dart:convert';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:white_crust/auth/verify.dart';
import 'package:white_crust/dashboard/home.dart';
import 'package:white_crust/data/custom_icons_icons.dart';
import 'package:white_crust/data/login_session.dart';
import '../data/custom_color.dart';
import '../data/custom_sizes.dart';
import '../data/custom_strings.dart';
import 'package:flutter/cupertino.dart';
import '../data/utils.dart';
import 'package:international_phone_input/international_phone_input.dart';
import 'reset/forgot.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPage createState() => _LoginPage();
}

class _LoginPage extends State<LoginPage> {

  TextEditingController passwordController = TextEditingController();
  bool _noShowPassword = true;
  ApiBaseHelper apiBaseHelper = new ApiBaseHelper();
  String phoneErr, passwordErr;
  LoginSessionManager loginSessionManager = new LoginSessionManager();
  String phoneNumber;
  String phoneIsoCode = '+234';
  String _token;
  Stream<String> _tokenStream;

  void onPhoneNumberChange(
      String number, String internationalizedPhoneNumber, String isoCode) {
    if(mounted)setState(() {
      phoneNumber = number;
      phoneIsoCode = isoCode;
    });
  }

  void setToken(String token) {
    if(mounted)setState(() {
      _token = token;
    });
  }

  void _validateData() async {
    String phone = phoneNumber;
    String password = passwordController.text;
    if(phoneNumber.length < 10 || phoneNumber.length > 10){
      phoneErr = "Please enter valid phone number";
    }else if(password.isEmpty || password.length < 8){
      passwordErr = CustomStrings.passwordLengthError;
    }else {
      final body = jsonEncode({'mobile': phone, 'password': password, 'firebase_token':_token});
      var response = await apiBaseHelper.patch(context, UrlLinks.login, body, await getSimpleHeader(), isLoader: true);
      if(response['status']){
        var user = jsonEncode(response['data']['user']);
        await loginSessionManager.setToken(response['data']['token']);
        await loginSessionManager.setOtherData(user);
        await loginSessionManager.logUserIn();
        Navigator.pushAndRemoveUntil(context, MaterialPageRoute(
          builder: (BuildContext context) => App(),
        ), (r) => false);
      }else {
        if(response['message'] == 'unverified'){
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (BuildContext context) =>
                    VerificationPage(user: response['data'])
            ),
          );
        }else showSuccess(context, text: response['message'], actionText: "Close", isError: true, onTap: (){
          Navigator.of(context).pop();
        });
      }
    }
    setState(() {
    });
  }

  Widget _input(TextEditingController controller, String hint, {bool isPassword: false, bool isEmail: false}) {
    return Container(
        child: TextField(
          style: GoogleFonts.quicksand(
            textStyle: TextStyle(
              fontWeight: FontWeight.w500,
              fontSize: CustomSize.normalText,
              color: Color(CustomColor.black2),
            )
          ),
          controller: controller,
          obscureText: isPassword ? _noShowPassword : false,
          keyboardType: isPassword ? TextInputType.text : isEmail ? TextInputType.emailAddress : TextInputType.text,
          decoration: InputDecoration(
            hintStyle: GoogleFonts.quicksand(
              textStyle: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: CustomSize.normalText,
                color: Color(CustomColor.black2).withOpacity(.5),
              )
            ),
            hintText: hint,
            contentPadding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Color(CustomColor.primary).withOpacity(.3)),
              borderRadius: BorderRadius.circular(5),
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Color(CustomColor.primary).withOpacity(.3)),
              borderRadius: BorderRadius.circular(5),
            ),
            border: OutlineInputBorder(
              borderSide: BorderSide(color: Color(CustomColor.primary).withOpacity(.3)),
              borderRadius: BorderRadius.circular(5),
            ),
            suffix: isPassword ? InkWell(
              onTap: () {
                if(mounted) setState(() {
                  _noShowPassword = !_noShowPassword;
                });
              },
              child: Icon(
                _noShowPassword
                    ? CupertinoIcons.eye_slash
                    : CupertinoIcons.eye, color: Color(CustomColor.accent),
              ),
            ) : null,
          ),
        )
    );
  }

  Widget _showErr(String error){
    return Text(error, style: GoogleFonts.quicksand(
      textStyle: TextStyle(
        fontWeight: FontWeight.w500,
        fontSize: CustomSize.smallText,
        color: Color(CustomColor.danger),
      )
    ));
  }

  Widget _entryField(){
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 20),
          Container(
            decoration: BoxDecoration(
                border: Border.all(color: Color(CustomColor.primary).withOpacity(.3)),
              borderRadius: BorderRadius.circular(5)
            ),
            padding: EdgeInsets.symmetric(horizontal: 10),
            child: InternationalPhoneInput(
              decoration: InputDecoration.collapsed(
                hintText: 'Phone number',
                hintStyle: GoogleFonts.quicksand(
                  textStyle: TextStyle(
                    fontSize: CustomSize.normalText,
                  )
                ),
                filled: false,
              ),
              onPhoneNumberChange: onPhoneNumberChange,
              initialPhoneNumber: phoneNumber,
              initialSelection: phoneIsoCode,
              enabledCountries: ['+234'],
              showCountryCodes: true,
              showCountryFlags: true,
            ),
          ),
          if(phoneErr != null) _showErr(phoneErr),
          SizedBox(height: 20),
          _input(passwordController, "Password (Required)", isPassword: true),
          if(passwordErr != null) _showErr(passwordErr),
          SizedBox(height: 20),
          GestureDetector(
              onTap: _validateData,
              child: Container(
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.symmetric(vertical: 15, horizontal: 20),
                decoration: BoxDecoration(
                  color: Color(CustomColor.primary),
                  borderRadius: BorderRadius.circular(5),
                ),
                child: Center(
                  child: Text("Sign In", style: GoogleFonts.quicksand(
                    textStyle: TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: CustomSize.buttonText,
                      color: Color(CustomColor.white),
                    )
                  )),
                ),
              )
          ),
        ],
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    FirebaseMessaging.instance.getToken().then(setToken);
    _tokenStream = FirebaseMessaging.instance.onTokenRefresh;
    _tokenStream.listen(setToken);
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;
    return AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.dark,
        child: Scaffold(
            appBar: AppBar(
              backgroundColor: Color(CustomColor.white),
              elevation: 0.0,
              iconTheme: IconThemeData(
                color: Color(CustomColor.primary), //change your color here
              ),
            ),
            body: GestureDetector(
              onTap: () {
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              onVerticalDragDown: (DragDownDetails details){
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              onVerticalDragStart: (DragStartDetails details){
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              child:Container(
                width: width,
                height: height,
                color: Color(CustomColor.white),
                child: SingleChildScrollView(
                  child: Container(
                      width: width,
                      height: height-140,
                      //margin: EdgeInsets.only(bottom: 70),
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Column(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Image.asset("assets/images/icon1.png", width: 80),
                          SizedBox(height: 10),
                          Text("Sign In to your account",
                              textAlign: TextAlign.center, style: GoogleFonts.quicksand(
                                textStyle: TextStyle(
                                  fontWeight: FontWeight.w500,
                                  fontSize: CustomSize.bigText,
                                  color: Color(CustomColor.primaryDark),
                                )
                              )),
                          _entryField(),
                          SizedBox(height: 20),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Expanded(
                                  child: Container(
                                    height: 1,
                                    color: Color(CustomColor.primaryDark).withOpacity(.5),
                                  )
                              ),
                              SizedBox(width: 30),
                              Text("Or",
                                  textAlign: TextAlign.center, style: GoogleFonts.quicksand(
                                    textStyle: TextStyle(
                                      fontWeight: FontWeight.w500,
                                      fontSize: CustomSize.bigText,
                                      color: Color(CustomColor.primaryDark),
                                    )
                                  )
                              ),
                              SizedBox(width: 30),
                              Expanded(
                                  child: Container(
                                    height: 1,
                                    color: Color(CustomColor.primaryDark).withOpacity(.5),
                                  )
                              ),
                            ],
                          ),
                          SizedBox(height: 20),
                          TextButton(
                            onPressed: (){
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (BuildContext context) =>
                                        ForgotPassPage()
                                ),
                              );
                            },
                            child: Text("Forgot password?", style: GoogleFonts.quicksand(
                              textStyle: TextStyle(
                                fontWeight: FontWeight.w500,
                                fontSize: CustomSize.bigText,
                                color: Color(CustomColor.primary),
                              )
                            )),
                          )
                        ],
                      )
                  ),
                ),
              ),
            )
        )
    );
  }
}