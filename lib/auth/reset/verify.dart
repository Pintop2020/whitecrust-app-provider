import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:white_crust/auth/reset/reset.dart';
import '../../data/custom_color.dart';
import '../../data/custom_sizes.dart';
import '../../data/custom_strings.dart';
import 'package:flutter/cupertino.dart';
import '../../data/utils.dart';
import 'package:pinput/pin_put/pin_put.dart';
import 'dart:io';

class VerificationPageReset extends StatefulWidget {
  VerificationPageReset({Key key, this.user}) : super(key: key);
  final user;
  @override
  _VerificationPageReset createState() => _VerificationPageReset();
}

class _VerificationPageReset extends State<VerificationPageReset> {

  final FocusNode _pinPutFocusNode = FocusNode();
  bool _isSub = false;
  TextEditingController _pinPutController = TextEditingController();
  BoxDecoration get _pinPutDecoration {
    return BoxDecoration(
      color: Color(CustomColor.white),
      borderRadius: BorderRadius.circular(15.0),
      border: Border.all(color: Color(CustomColor.offBlack).withOpacity(.5), width: 1.0),
    );
  }
  ApiBaseHelper apiBaseHelper = new ApiBaseHelper();


  Widget _pinInputs(){
    return Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(vertical: 20, horizontal: 10),
        child: PinPut(
          textStyle: GoogleFonts.quicksand(
            textStyle: TextStyle(
              fontWeight: FontWeight.w500,
              fontSize: CustomSize.normalText,
              color: Color(CustomColor.black),
            )
          ),
          keyboardType: TextInputType.number,
          focusNode: _pinPutFocusNode,
          obscureText: '●',
          fieldsCount: 6,
          eachFieldWidth: Platform.isAndroid ? 40 : 50,
          eachFieldHeight: Platform.isAndroid ? 50 : 60,
          controller: _pinPutController,
          submittedFieldDecoration: _pinPutDecoration.copyWith(
            borderRadius: BorderRadius.circular(20.0),
          ),
          selectedFieldDecoration: _pinPutDecoration,
          followingFieldDecoration: _pinPutDecoration.copyWith(
            borderRadius: BorderRadius.circular(5.0),
          ),
        )
    );
  }

  void _validateData() async {
    String value = _pinPutController.text;
    String did = "${widget.user['id']}";
    var response = await apiBaseHelper.get(context, UrlLinks.check_token+'?user_id=${widget.user['id']}&token=$value', await getSimpleHeader(), isLoader: true);
    if(mounted)setState(() {
      _isSub = false;
    });
    _pinPutController.clear();
    if(response['status']){
      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (BuildContext context) =>
                ResetPasswordPage(id: widget.user['id'].toString())
        ),
      );
    }else{
      showAlert(context, title: CustomStrings.oops, message: response['message']);
    }
  }

  @override
  void initState() {
    super.initState();
    _pinPutController.addListener((){
      if(_pinPutController.text.length < 7 && _pinPutController.text.length > 5 && !_isSub){
        if(mounted)setState(() {
          _pinPutFocusNode.unfocus();
          _isSub = true;
        });
        _validateData();
      }else {
        if(mounted)setState(() {
          _isSub = false;
        });
      }
    });
  }


  @override
  void dispose() {
    _pinPutController.dispose();
    super.dispose();
  }

  void _createToken() async {
    var response = await apiBaseHelper.get(context, UrlLinks.create_token+'?user_id=${widget.user['id']}&type=mobile', await getSimpleHeader(), isLoader: true);
    _pinPutController.clear();
    if(!response['status']){
      showAlert(context, title: CustomStrings.oops, message: response['message']);
    }else showInSnackBar(context, response['message'], Color(CustomColor.black2), Color(CustomColor.success));
  }

  Widget _entryField(){
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 20),
          _pinInputs(),
          SizedBox(height: 20),
          GestureDetector(
              onTap: _createToken,
              child: Center(
                  child: Text("Resend Code", style: GoogleFonts.quicksand(
                    textStyle: TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: CustomSize.normalText,
                      color: Color(CustomColor.accent),
                    )
                  ))
              )
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;
    return AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.dark,
        child: Scaffold(
            appBar: AppBar(
              backgroundColor: Color(CustomColor.white),
              elevation: 0.0,
              iconTheme: IconThemeData(
                color: Color(CustomColor.primary), //change your color here
              ),
              leading: IconButton(
                icon: Icon(Icons.arrow_back, color: Color(CustomColor.offBlack2)),
                onPressed: () => Navigator.of(context).pop(),
              ),
            ),
            body: GestureDetector(
              onTap: () {
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              onVerticalDragDown: (DragDownDetails details){
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              onVerticalDragStart: (DragStartDetails details){
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              child:Container(
                width: width,
                height: height,
                color: Color(CustomColor.white),
                child: Container(
                    width: width,
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 20),
                          child: Text("Reset Verification", textAlign: TextAlign.center,style: GoogleFonts.quicksand(
                            textStyle: TextStyle(
                              fontWeight: FontWeight.w800,
                              fontSize: CustomSize.bigTitle,
                              color: Color(CustomColor.offBlack),
                            )
                          )),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 20),
                          child:  Text("Please enter the token sent to your mobile phone to proceed to resetting your password.",
                              textAlign: TextAlign.left,style: GoogleFonts.quicksand(
                                textStyle: TextStyle(
                                  fontWeight: FontWeight.w500,
                                  fontSize: CustomSize.normalText,
                                  color: Color(CustomColor.info),
                                )
                              )),
                        ),
                        SizedBox(height: 20),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 20),
                          child: _entryField(),
                        ),
                      ],
                    )
                ),
              ),
            )
        )
    );
  }
}