import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import '../../data/custom_color.dart';
import '../../data/custom_sizes.dart';
import '../../data/custom_strings.dart';
import 'package:flutter/cupertino.dart';
import '../../data/utils.dart';
import 'verify.dart';
import 'package:international_phone_input/international_phone_input.dart';

class ForgotPassPage extends StatefulWidget {
  ForgotPassPage({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _ForgotPassPage createState() => _ForgotPassPage();
}

class _ForgotPassPage extends State<ForgotPassPage> {

  ApiBaseHelper apiBaseHelper = new ApiBaseHelper();
  String phoneErr, passwordErr;
  String phoneNumber;
  String phoneIsoCode = '+234';

  void onPhoneNumberChange(
      String number, String internationalizedPhoneNumber, String isoCode) {
    if(mounted)setState(() {
      phoneNumber = number;
      phoneIsoCode = isoCode;
    });
  }

  void _validateData() async {
    String phone = phoneNumber;
    if(phoneNumber.length < 10 || phoneNumber.length > 10){
      phoneErr = "Please enter valid phone number";
    }else {
      final body = jsonEncode({'mobile': phone});
      var response = await apiBaseHelper.post(context, UrlLinks.forget, body, await getSimpleHeader(), isLoader: true);
      if(response['status']){
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (BuildContext context) => VerificationPageReset(user: response['data']),
          ),
        );
      }else{
        showSuccess(context, text: response['message'], actionText: "Close", isError: true, onTap: (){
          Navigator.of(context).pop();
        });
      }
    }
  }

  Widget _showErr(String error){
    return Text(error, style: GoogleFonts.quicksand(
      textStyle: TextStyle(
        fontWeight: FontWeight.w500,
        fontSize: CustomSize.smallText,
        color: Color(CustomColor.danger),
      )
    ));
  }

  Widget _entryField(){
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 20),
          Container(
            decoration: BoxDecoration(
                border: Border.all(color: Color(CustomColor.primary).withOpacity(.3)),
                borderRadius: BorderRadius.circular(5)
            ),
            padding: EdgeInsets.symmetric(horizontal: 10),
            child: InternationalPhoneInput(
              decoration: InputDecoration.collapsed(
                hintText: 'Phone number',
                hintStyle: GoogleFonts.quicksand(
                  textStyle: TextStyle(
                    fontSize: CustomSize.normalText,
                  )
                ),
                filled: false,
              ),
              onPhoneNumberChange: onPhoneNumberChange,
              initialPhoneNumber: phoneNumber,
              initialSelection: phoneIsoCode,
              enabledCountries: ['+234'],
              showCountryCodes: true,
              showCountryFlags: true,
            ),
          ),
          if(phoneErr != null) _showErr(phoneErr),
          SizedBox(height: 20),
          GestureDetector(
              onTap: _validateData,
              child: Container(
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.symmetric(vertical: 15, horizontal: 20),
                decoration: BoxDecoration(
                  color: Color(CustomColor.primary),
                  borderRadius: BorderRadius.circular(5),
                ),
                child: Center(
                  child: Text("Get reset token", style: GoogleFonts.quicksand(
                    textStyle: TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: CustomSize.buttonText,
                      color: Color(CustomColor.white),
                    )
                  )),
                ),
              )
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;
    return AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.dark,
        child: Scaffold(
            appBar: AppBar(
              backgroundColor: Color(CustomColor.white),
              elevation: 0.0,
              iconTheme: IconThemeData(
                color: Color(CustomColor.primary), //change your color here
              ),
            ),
            body: GestureDetector(
              onTap: () {
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              onVerticalDragDown: (DragDownDetails details){
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              onVerticalDragStart: (DragStartDetails details){
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              child:Container(
                width: width,
                height: height,
                color: Color(CustomColor.white),
                child: SingleChildScrollView(
                  child: Container(
                      width: width,
                      height: height-140,
                      //margin: EdgeInsets.only(bottom: 70),
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Column(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Image.asset("assets/images/icon1.png", width: 80),
                          SizedBox(height: 10),
                          Text("Reset my account password",
                              textAlign: TextAlign.center, style: GoogleFonts.quicksand(
                                textStyle: TextStyle(
                                  fontWeight: FontWeight.w500,
                                  fontSize: CustomSize.bigText,
                                  color: Color(CustomColor.primaryDark),
                                )
                              )),
                          _entryField(),
                          SizedBox(height: 20),
                        ],
                      )
                  ),
                ),
              ),
            )
        )
    );
  }
}