import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:white_crust/auth/login.dart';
import 'package:white_crust/data/custom_icons_icons.dart';
import '../../data/custom_color.dart';
import '../../data/custom_sizes.dart';
import '../../data/custom_strings.dart';
import 'package:flutter/cupertino.dart';
import '../../data/utils.dart';

class ResetPasswordPage extends StatefulWidget {
  ResetPasswordPage({Key key, this.id}) : super(key: key);
  final String id;
  @override
  _ResetPasswordPage createState() => _ResetPasswordPage();
}

class _ResetPasswordPage extends State<ResetPasswordPage> {

  TextEditingController passwordController = TextEditingController();
  bool _noShowPassword = true;
  ApiBaseHelper apiBaseHelper = new ApiBaseHelper();

  void _validateData() async {
    String password = passwordController.text;
    if(password.isEmpty || password.length < 8){
      showAlert(context, title: CustomStrings.oops, message: CustomStrings.passwordLengthError);
    }else {
      final body = jsonEncode({'password': password, 'user_id': widget.id});
      var response = await apiBaseHelper.post(context, UrlLinks.reset, body, await getSimpleHeader(), isLoader: true);
      if(response['status']){
        showSuccess(context, text: response['message'], actionText: "Login Now", onTap: (){
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (BuildContext context) =>
                    LoginPage()
            ),
          );
        });
      }else{
        showAlert(context, title: CustomStrings.oops, message: response['message']);
      }
    }
  }

  Widget _input(TextEditingController controller, String label) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(label, style: GoogleFonts.quicksand(
            textStyle: TextStyle(
              fontWeight: FontWeight.w500,
              fontSize: CustomSize.normalText,
              color: Color(CustomColor.offBlack).withOpacity(.5),
            )
          )),
          SizedBox(height: 10),
          TextField(
            style: GoogleFonts.quicksand(
              textStyle: TextStyle(
                fontWeight: FontWeight.w800,
                fontSize: CustomSize.normalText,
                color: Color(CustomColor.offBlack),
              )
            ),
            controller: controller,
            obscureText: _noShowPassword,
            keyboardType: TextInputType.text,
            decoration: InputDecoration(
              filled: true,
              fillColor: Color(CustomColor.offBlack2).withOpacity(.2),
              contentPadding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Color(CustomColor.offBlack2).withOpacity(.3), width: 0),
                borderRadius: BorderRadius.circular(5),
              ),
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Color(CustomColor.offBlack2).withOpacity(.3), width: 0),
                borderRadius: BorderRadius.circular(5),
              ),
              border: OutlineInputBorder(
                borderSide: BorderSide(color: Color(CustomColor.offBlack2).withOpacity(.3), width: 0),
                borderRadius: BorderRadius.circular(5),
              ),
              suffix: InkWell(
                onTap: () {
                  if(mounted) setState(() {
                    _noShowPassword = !_noShowPassword;
                  });
                },
                child: Icon(
                  _noShowPassword
                      ? CupertinoIcons.eye_slash
                      : CupertinoIcons.eye, color: Color(CustomColor.accent),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _entryField(){
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 20),
          _input(passwordController, CustomStrings.passwordLabel),
          SizedBox(height: 20),
          GestureDetector(
              onTap: _validateData,
              child: Container(
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.symmetric(vertical: 15, horizontal: 20),
                decoration: BoxDecoration(
                  color: Color(CustomColor.primary),
                  borderRadius: BorderRadius.circular(5),
                ),
                child: Center(
                  child: Text("Reset now", style: GoogleFonts.quicksand(
                    textStyle: TextStyle(
                      fontWeight: FontWeight.w600,
                      fontSize: CustomSize.buttonText,
                      color: Color(CustomColor.white),
                    )
                  )),
                ),
              )
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final height = MediaQuery.of(context).size.height;
    return AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.dark,
        child: Scaffold(
            appBar: AppBar(
              backgroundColor: Color(CustomColor.white),
              elevation: 0.0,
              iconTheme: IconThemeData(
                color: Color(CustomColor.primary), //change your color here
              ),
            ),
            body: GestureDetector(
              onTap: () {
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              onVerticalDragDown: (DragDownDetails details){
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              onVerticalDragStart: (DragStartDetails details){
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              child:Container(
                width: width,
                height: height,
                color: Color(CustomColor.white),
                child: SingleChildScrollView(
                  child: Container(
                      width: width,
                      height: height-140,
                      //margin: EdgeInsets.only(bottom: 70),
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Column(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Image.asset("assets/images/icon1.png", width: 80),
                          SizedBox(height: 10),
                          Text("Create new passwords",
                              textAlign: TextAlign.center, style: GoogleFonts.quicksand(
                                textStyle: TextStyle(
                                  fontWeight: FontWeight.w500,
                                  fontSize: CustomSize.bigText,
                                  color: Color(CustomColor.primaryDark),
                                )
                              )),
                          _entryField(),
                          SizedBox(height: 20),
                        ],
                      )
                  ),
                ),
              ),
            )
        )
    );
  }
}